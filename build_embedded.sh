#!/bin/bash


key="$1"
case $key in
	clean)
	cd build/images/cortex-A9
        cmake -DBUILD_TYPE=cortex-A9 -DCMAKE_TOOLCHAIN_FILE=toolchain-arm-linux.cmake ../../../
        make clean
        RETURN_VALUE=$?
        cd ../../../
	;;
	*)
	cd build/images/cortex-A9
        cmake -DBUILD_TYPE=cortex-A9 -DCMAKE_TOOLCHAIN_FILE=toolchain-arm-linux.cmake ../../../
	make -j 12
	RETURN_VALUE=$?
	cd ../../../
        [ -e build/output/cortex-A9/bin/source.sha1 ] && rm build/output/cortex-A9/bin/source.sha1
        [ -e build/output/cortex-A9/bin/signature.sha1 ] && rm build/output/cortex-A9/bin/signature.sha1
        cd build/output/cortex-A9/bin
        [ ! -d ../patching ] && mkdir ../patching
        mv bsdiff ../patching/bsdiff && mv bspatch ../patching/bspatch
        [ -e signature.sha1 ] && rm *.sha1
        sha1sum * > signature.sha1
        git log --pretty=format:'%H' -n 1 > source.sha1
        printf "\n" >> source.sha1
        echo "Checksum file generated" && cat signature.sha1
        echo " "
        echo "source sha1: " && cat source.sha1
        echo " "
        cd ../../../../
	;;
esac
exit $RETURN_VALUE
