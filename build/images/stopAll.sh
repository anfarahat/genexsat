#!/bin/bash

killall tlm_app
killall obc_app
killall adcs_app
killall router_app
killall obt_app


rm -rf /dev/mqueue/*
rm -rf /dev/shm/sem.*
rm -rf /dev/shm/obt_shmem
