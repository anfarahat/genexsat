#!/bin/bash

if [ $# -eq 0 ]; then
   echo "Usage: compute_patch <git-sha1-old> [git-sha1-new]"
   exit 1
fi

# Checkout the first argument hash
# build the tree
# copy the output into its own directory
SHA_OLD=$1
if [ $# -eq 2 ]; then
    SHA_NEW=$2
else
    SHA_NEW=`git log --pretty=format:'%H' -n 1`
fi
echo ""
echo "New revision  ${SHA_NEW}, old revision ${SHA_OLD}"
echo ""
cd ../..


git checkout ${SHA_OLD}
./build_embedded.sh
rm -rf build/output/cortex-A9/old_bin
[ ! -d build/output/cortex-A9/old_bin ] && mkdir build/output/cortex-A9/old_bin
cp build/output/cortex-A9/bin/* build/output/cortex-A9/old_bin/

# Checkout the second argument
# build the tree
# copy the output into its own directory
git checkout ${SHA_NEW}
./build_embedded.sh
rm -rf build/output/cortex-A9/new_bin
[ ! -d build/output/cortex-A9/new_bin ] && mkdir build/output/cortex-A9/new_bin
cp build/output/cortex-A9/bin/* build/output/cortex-A9/new_bin/



# Compute the binary patch using bsdiff between the executables
# from each directory
# Form the software update package using the following:
## 1- The generated diffs
## 2- The sha-1 files of the old and the new versions of executable
## 3- The old and new source-sha1 hashrm -rf build/output/cortex-A9/patch
[ ! -d build/output/cortex-A9/patch ] && mkdir build/output/cortex-A9/patch

cd build/output/cortex-A9/old_bin
for file in *
do
    bsdiff "${file}" "../new_bin/${file}" "../patch/${file}.patch"
done

cd ../../../../

rm build/output/cortex-A9/patch/signature.sha1.patch
rm build/output/cortex-A9/patch/source.sha1.patch

cp build/output/cortex-A9/old_bin/signature.sha1 build/output/cortex-A9/patch/signature_old.sha1
cp build/output/cortex-A9/old_bin/source.sha1    build/output/cortex-A9/patch/source_old.sha1
cp build/output/cortex-A9/new_bin/signature.sha1 build/output/cortex-A9/patch/signature_new.sha1
cp build/output/cortex-A9/new_bin/source.sha1    build/output/cortex-A9/patch/source_new.sha1


rm -rf build/output/cortex-A9/old_bin
rm -rf build/output/cortex-A9/new_bin


cd build/scripts
git checkout master 

# compress the software update package folder

