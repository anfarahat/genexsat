\documentclass[12pt]{article}


%%\usepackage{geometry}
\usepackage[cm]{fullpage}

\title {\textbf{NexSAT Project} \\ \textit{Platform Control Top Level Architecture \\and \\Development Timeline}}
\author{Aly Farahat, Ph.D. \\ Software and Systems Architect}

\date{}


\begin{document}
\maketitle


\begin{abstract}
In this document, we present a summary of the top level High Performance Onboard Computer (HP-OBC) software architecture of NExSAT. In addition, we demonstrate a tentative software development and testing schedule/assignment.
\end{abstract}
\newpage

\tableofcontents
\newpage

\section{Introduction}
The main goal of any architectural decisions presented in this document are long term reuse of software component. Given the tight timeline, we are presenting a very ambitious plan that we consider tentative. We bare in mind the following goals throughout the development and testing activities.
\begin{itemize}
\item Create a reusable set of software components across different projects,
\item Transfer software development technology to engineers and team leads,
\item Simplify the design and features as much as possible given the tight schedule.
\end{itemize}


\section{Spacecraft Platform Common Features}
Taking into consideration the long term applicability of the created software components, we list the main features that end up in a spacecraft that are relevant to our project and its schedule.
\begin{itemize}
\item Attitude Determination and Control: the task of determining and controlling the orientation of the spacecraft in real-time.
\item Telemetry Gathering, Storing and Transmission.
\item Telecommand real-time execution and stored command execution.
\item State machine implementing different modes of operation and their switching logic.
\end{itemize}

\section{Software Layers and Libraries}
This section details the various software layers and the libraries that belong to each layer.
\subsection{Layers}
\begin{enumerate}
\item \emph{Application Layer}: where mission specific features are implemented. This includes some of the ADCS functionality, Telecommand and Telemetry functionality. Direct application features are implemented in that layer. In addition, different modes of operation are also in this layer.
\item \emph{Middleware Layer}: where generic algorithms and mission independent portions of the features take place. This includes most of the ADCS algorithms, state machine infrastructure, logical layers of network protocols and master event handler mechanisms. 

Middleware layer also include totally generic algorithms for implementing queues, state machines, set operations and complex mathematical libraries.

\item \emph{Platform Layer}: where specific hardware dependent or operating system-dependent code is implemented. A unified abstraction interface is provided to  libraries that belong to this layer. As such, application or middleware layer need not worry about hardware or operating system implementation details. Many of the utilities in this layer include timers, serial-port interface GPIO interface device drivers and interrupt handlers.
\end{enumerate}
\subsection{Libraries}

A library is the basic reusable building block in across several applications. A library is defined by its interface and the contract it provides to its environment. A library could have multiple implementations that depend on the following.
\begin{enumerate}
\item Application requirements,
\item platform requirements.
\end{enumerate}
We propose that modules in the middleware and platform layers be compiled as libraries. Modules in the application layer will be generated as object files and reused individually.

The following list of libraries/modules is proposed for each layer.

\begin{enumerate}
\item \textbf{Application Layer}
\begin{itemize}
    \item ADCS mission specific library (ADCSMS): includes all the specific features of ADCS that are not reusable across different missions and that proper to NExSat.
    \item Command manager mission specific library (CMDMS). Includes all the ground control command execution functions.
    \item Telemetry manager mission specific library (TLMMS). Includes specific format definitions of all the telemetry packets from all subsystems. Also includes any specific sampling requirements for different modes of operation.
    \item Onboard-Time mission specific library (OBTMS). Includes allocation of shared memory for onboard time across all the processes. Also creates a high resolution timer for that purporse.
    \item Mode Switcher Library (MS): The decision to support a centralized vs. a distributed state has not been finalized yet. A centralized state is a single component where all the satellite modes are kept and handled. A distributed mode creates a state machine for each component. For a distributed state, we shall assume three different state machines, one per each application library.
    \begin{enumerate}
    \item Telemetry manager state machine.
    \item ADCS state machine,
    \item Imaging state machine
    \end{enumerate}
    
    A centralized state machine is easier to maintain and more optimized for the number of global states. In addition, it avoids the overhead of synchrnizing local states of distributed state machines.
    
    \item Basic monitoring and reaction to faults.
    \item Software Loading and restart library.
\end{itemize}

\item \textbf{Middleware}
\begin{itemize}
\item Core onboard-time library (OBT).
\item Core telemetry manager, common to all missions (TLMM).
\item Stored command manager and base infrastructure for command execution (CMDM).
\item Main event handler library (EVT). This library has the goal of running the event detection background loop to properly route events to their corresponding software module.
\item Remote Procedure Call (RPC) library to enable communication between different applications. This essentially is an abstraction to the Linux system calls enabling RPC.
\item Core ADCS algorithms (ADCS).
\item Core math library functions, if needed (MATH).
\item Messaging protocol defined by iBST (MSG).
\item Core state machine infrastructure (STM).
\item Third Party Libraries
\begin{enumerate}
\item Protobuf: for message format specification, parsing and accessing.
\item GTest: for unit testing framework.
\item CCSDS: any trusted third party library for CCSDS
\end{enumerate} 
\end{itemize}

\item \textbf{Platform Specific Layer}
\begin{itemize}
\item Pulse Width Modulation (PWM) device driver and hardware abstraction library.
\item Serial port device driver and hardware abstraction library.
\item GPIO device driver and hardware abstraction library.
\item Flash IO hardware abstraction library.
\end{itemize}
\end{enumerate}


\section{Applications and Inter-Process Communication (IPC)}

We choose to run the main platform features as independent applications/processes hosted by Linux Operating System. Independent applications have the benefit of failing independently, thus increasing the reliability/availability of their provided features.

One drawback of having several processes implementing interrelated features is providing solid means of communicating between several processes. We choose to use a custom interface using message queues.

The list of applications running on HP-OBC with their corresponding libraries are the following.
\begin{enumerate}
\item ADCS Service, which consists of the following libraries: ADCSMS, ADCS, RPC, MSG, EVT, platform libraries.

\item Telemetry Service, which consists of the following libraries: TLMMS, TLMM, protobuf, MSG, RPC, EVT, platform libraries.
\item Telecommand Service:  CMDM, protobuf, MSG, RPC, EVT, platform libraries.
\item Onboard-time Service: OBTMS, OBT, platform libraries.
\item Master Mode Switcher Service: STM, EVT,  
\item SW-Update Service. TBD
\end{enumerate}

 
\section{Develpoment Environment and Build System}
We propose the use of C\texttt{++} as our primary development language. The rationale behind choosing C\texttt{++} over C is justified as such.

\begin{enumerate}
\item C\texttt{++} is an Object-Oriented language, thereby enforcing a minimal level of modularity and clean software engineering practice in the code.
\item C\texttt{++} has an extensive Standard Template Library (STL) to support diverse functions.
\item C\texttt{++} is a compilable language that produces optimized object code if well utilized.
\end{enumerate}

The build environment is a Linux-based environment using platform-agnostic \emph{cmake} toolset. Cmake allows modular software engineering together with a build system for multiple targets that easily cross-compiles and links for multiple targets. Our aim is to produce two targets.
\begin{enumerate}
\item Desktop Target: this executable is useful for quick testing and debugging on the desktop. All code in platform layer has to be rewritten for that layer to allow execution. Thus, the desktop target is essentially for integrated testing of middleware and platform layer.

\item Embedded Target: this executable is the product used on the ARM-9 board and running in flight. 
\end{enumerate}

We shall adopt \emph{Google Test} as our unit testing framework. Unit tests runs independently on the desktop to verify the correctness of application and middleware libraries. Every developer \emph{is required} to write unit tests early on during the development of his code/functions. Unit tests are essential to determine the correctness of basic of functionality and behavior of basic objects created by developers.

\section{Development/Testing Workflow and Timeline}
In this section, we detail the basics of the team workflow and work organization. We finally present the general task assignment and deliverables timeline.

\subsection{Development and Testing Workflow} 
\begin{itemize}
\item Development environment is Linux desktop-based. We shall use Ubuntu LTS 16.04 as our host environment.
\item Build toolchains are for both desktop Linux and embedded ARM9 core.
\item Our debugger of choice is GDB-based. Developers are free to choose their host environment for debugging their code. Debugging on the embedded board will happen by running GDB on the host machine. If needed, a gdb server could run on the embedded target with a client running on the host PC. Developers are expected to familiarize themselves with GDB command line version. Otherwise, they are free to choose a GUI running on their host PC on top of their GDB clien. Same set of tools is applicable for desktop builds.
\item We shall adopt \emph{Git} as our tool of choice for version controlling our source code and build scripts. Third-party libraries are imported as \emph{Git Submodules} and build as part of our build system.
\item Each developer has access to the shared Bitbucket repository that hosts NExSat code-base.
\item Each developer has to have stable access to Internet; this is indispensable to the workflow.
\item Each developer is assigned tasks on Bitbucket issue tracker. The developer creates a \emph{Git Branch} named after the issue ID called a feature branch. The feature branch starts of the most stable development and release branch. After the developer is confident about his updates and unit tests, he pushes his updates upstream and creates a \emph{Pull Request} on Bitbucket for further review and merge into the stable development branch. After addressing review comments and further testing, the developer merges his changes contingent on the final approval by the reviewer.
\item No developer shall be given \emph{write-access} to the main development branch. The only way to update the development branch is through pull-requests.
\end{itemize}

\subsection{Development Assignment and Testing Timeline}

Based on our proposed software architecture, we plan the delivery according to the following timeline.

\begin{enumerate}
\item \textbf{Build System, Tree Structure and all interfaces for all libraries (draft), many of basic middleware components}: August $15^{th}$, 2018 (Aly Farahat).
\item \textbf{OBTSM, OBT}: August $31^{st}$, 2018 (Aly Farahat)
\item \textbf{MSG, RPC and EVT}: September $30^{th}$, 2018 (Aly Farahat, Mohammad El-Mahdy (optional))
\item \textbf{TLMSM, TLM} (Draft Version): September $30^{th}$, 2018 (Eman El-Emam, Sara Ragheb)
\item \textbf{CMDSM, CMD} (Draft Version): September $30^{th}$, 2018 (Abu-Bakr, Ranya Salah)
\item \textbf{ADCSSM, ADCS} (Draft Version): September $30^{th}$, 2018 (Ahmed Farrag, Safaa Ibrahim, Hadeer),
\item \textbf{Platform Dependent Libraries}: August $31^{st}$, 2018 (Mohammad El-Mahdy, Abu-Bakr (\emph{optional}))
\item \textbf{Mode Switching Logic} (Draft Version): September $30^{th}$, 2018 (Eman El-Emam, Abu-Bakr).
\item \textbf{Unit Tests Complete -- Start of Software Integration}: October $15^{st}$, 2018.
\item \textbf{Draft Version Release -- Start Fully Autonomous Tests}: November $15^{th}$, 2018.
\item \textbf{First Engineering Release -- Integration Tests Start}: January $1^{st}$, 2019.
\item \textbf{Official Engineering Release -- Integration Tests}: March $1^{st}$, 2019.
\item \textbf{Preflight Release -- End of Tests}: Final week of March, 2019. 
\end{enumerate}

\section{Conclusions}

We presented an overview of the top level architecture of NExSAT software. Modularity, reusability and simplicity are the main driving factors for delivering the above software components. We demonstrated an ambitious tentative plan for the project deliverables and testing schedule.


\end{document}