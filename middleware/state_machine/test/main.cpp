#include "application.h"
#include "state_machine.h"

int main (int argv, char * argc[])
{
    Application app("obcApp");
    enum State {
        INIT = 0,
        FIRST = 1,
        SECOND = 2,
        THIRD = 3
    };
    enum Event {
        STAY = 0,
        MOVE = 1
    };   

    StateMachine & SM = StateMachine::getInstance("TestSM", INIT);

    SMTransition transition;
    SM.addTransition (INIT, STAY, INIT, nullptr);
    SM.addTransition (FIRST, STAY, FIRST, nullptr);
    SM.addTransition (SECOND, STAY, SECOND, nullptr);
    SM.addTransition(THIRD, STAY, THIRD, nullptr);
    SM.addTransition (INIT, MOVE, FIRST, nullptr);
    SM.addTransition (FIRST, MOVE, SECOND, nullptr);
    SM.addTransition (SECOND, MOVE, THIRD, nullptr);
    SM.addTransition(THIRD, MOVE, INIT, nullptr);

    
    SM.dispatchEvent(MOVE);
    SM.dispatchEvent(STAY); 
    SM.dispatchEvent(MOVE);
    SM.dispatchEvent(STAY);
    SM.dispatchEvent(MOVE);
    SM.dispatchEvent(STAY); 
    SM.dispatchEvent(MOVE);
    SM.dispatchEvent(STAY);
    app.run();

    return 0;
}
