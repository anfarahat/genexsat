#pragma once
#include <cstdint>
#include <string>

typedef uint32_t SMEvent;
   // We are going to use an event named queue as a reactive object to all state machines.
   // This way, we guarantee that all events get delivered in a timely manner without worrying
   // much about thread safety.

typedef uint32_t SMState;

class StateMachine
{
public:
    virtual SMState getCurrentState(uint32_t smIndex = 0) const = 0;
    // Writes to a reactive msg queue
    virtual bool dispatchEvent(SMEvent event, 
                               uint32_t smIndex = 0) = 0; // called to send an event the the input
                                                          // queue of the state machine.
    virtual bool addTransition(SMState sourceState,
                               SMEvent event,
                               SMState destState,
                               void * (*callback)(void *) = nullptr) = 0; // This is for all state machines and is not index specific.
                                                                          // Ideally the specification fo the transition function
                                                                          // is part of the constructor.

    static StateMachine & getInstance(std::string name, SMState initialState = 0, uint32_t numOfMachines = 1);
};

