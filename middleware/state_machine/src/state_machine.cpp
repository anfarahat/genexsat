#include "state_machine.h"

#include "narss_msg_queue.h"
#include "event_reactor.h"

#include <unordered_map>
#include <map>
#include <vector>
#include <cassert>

class StateMachineImpl : public StateMachine
{
public:
    constexpr static uint32_t MAX_NUM_MSGS = 16;
    struct QueueEvent {SMEvent event; uint32_t smIndex;};

    static void * queueCallback(void *); // This is where 
                                         // Queue is flushed
                                         // and state transitions
                                         // are applied.
    
    StateMachineImpl(std::string name, SMState initialState, 
                     uint32_t numOfMachines) 
    : name_(name),
      currentState_(numOfMachines, initialState),
      inputQueue_( MsgQueue::getInstance (name+"EvtQueue", 
                                          sizeof(QueueEvent), 
                                          MAX_NUM_MSGS) ) {
        EventReactor::getSingleton().registerCallback(inputQueue_,
                                                      queueCallback,
                                                      this); // Modify
                                                                     // to pass
                                                                     // input args
    }

    ~StateMachineImpl() {
        assert (MsgQueue::removeInstance(name_));
    }

    SMState getCurrentState(uint32_t smIndex) const override;
    bool dispatchEvent(SMEvent event, uint32_t smIndex) override;
    bool addTransition(SMState sourceState,
                       SMEvent event,
                       SMState destState,
                       void * (*callback)(void *));

    
private:
    void applyEvent (SMEvent event, uint32_t smIndex = 0);
    std::string name_;
    std::vector<SMState> currentState_;
    struct SourcePair {
        SMState source; 
        SMEvent event;
        bool operator <(const SourcePair& sPair) const {
            return (this->source < sPair.source ||
                    (this->source == sPair.source && this->event < sPair.event));
        }
    };
    struct TargetPair {SMState target; void * (*callback)(void*);};
    std::map<SourcePair, TargetPair> transitionTable_;
    MsgQueue & inputQueue_;
};

StateMachine & StateMachine::getInstance(std::string name, SMState initialState,
                                         uint32_t numOfMachines) {
    static std::unordered_map<std::string, StateMachine *> SMRegistry;

    if (SMRegistry.find(name) == SMRegistry.end()) {
        SMRegistry [name] = new StateMachineImpl(name, 
                                                 initialState,
                                                 numOfMachines);
    }

    return *SMRegistry [name];
}


SMState StateMachineImpl::getCurrentState (uint32_t smIndex) const
{
    assert(smIndex < currentState_.size());
    return currentState_[smIndex];
}

bool StateMachineImpl::dispatchEvent (SMEvent event, uint32_t smIndex)
{
    assert (smIndex < currentState_.size());

    QueueEvent qEvent = {event, smIndex};
    
    return inputQueue_.sendMsg((void *)&qEvent, sizeof(qEvent), MsgQueue::LOW); 
}

void * StateMachineImpl::queueCallback(void * SM)
{
    // args should be a pointer to the state machine object
    // Need to modify Reactor to allow passing args
    // in to the reactor.
    StateMachineImpl * this_ = reinterpret_cast<StateMachineImpl *>(SM);
    MsgQueue & eQueue = this_->inputQueue_;
    
    do {
        // Get event
        QueueEvent qEvent;
        uint32_t len;

        eQueue.rcvMsg(&qEvent, len);
        assert (len == sizeof (QueueEvent));
        this_->applyEvent (qEvent.event, qEvent.smIndex);
    } while (eQueue.getNumMsgs() > 0);
    
}

bool StateMachineImpl::addTransition(SMState sourceState,
                                     SMEvent event,
                                     SMState destState,
                                     void * (*callback)(void *))
{
    bool ret = false;

    SourcePair sPair = {sourceState, event};
    TargetPair tPair = {destState, callback};
    
    if (transitionTable_.find(sPair) == transitionTable_.end()) {
        transitionTable_ [sPair] = tPair;
        ret = true;
    }

    return ret;
}

void StateMachineImpl::applyEvent (SMEvent event, uint32_t smIndex)
{
    SourcePair sPair = {currentState_[smIndex], event};
    
    // Do not do anything if the state and transition are not specified.
    if (transitionTable_.find(sPair) == transitionTable_.end()) {
       return;
    }

    // Call transition function
    if (transitionTable_[sPair].callback != nullptr) {
        transitionTable_[sPair].callback(nullptr);
    }

    currentState_[smIndex] = transitionTable_[sPair].target;
}
