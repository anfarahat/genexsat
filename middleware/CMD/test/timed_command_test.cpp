
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "timed_command.h"
#include "onboard_time.h"
#include "cmd_manager.h"
#include "router.h"


// For now, create the mock here
// Ideally, it should live in the test directory
// of the host library. It should only be mocked once!
using ::testing::_;
using ::testing::AtLeast;
using ::testing::Return;

class MockOnboardTime : public OnboardTime {
public:
    MockOnboardTime()
    : onboardTime_(0){}
    int64_t getTime() override {
        return onboardTime_;
    }

    void advanceTime() override {
        onboardTime_ += 1000;
    }

    MOCK_METHOD1(setTime, void(int64_t timeValue));
    // MOCK_METHOD0(advanceTime, void());
    MOCK_METHOD0(getNumOfThreads, uint32_t &());
    MOCK_CONST_METHOD0(isMaster, bool());
    MOCK_METHOD1(getPeriodRef, uint32_t & (uint32_t threadId));

private:
    uint64_t onboardTime_;
};

OnboardTime & OnboardTime::getSingleton(bool isMaster, uint32_t tick) {
    static MockOnboardTime mockObt;
    isMaster = isMaster;
    tick = tick;
    return mockObt;
}

class MockRoute : public Router {
public:
    MOCK_METHOD1(routeMsg, bool (InternalMsg & msg));
    MOCK_METHOD2(getFrameLength,uint16_t (uint32_t cmdCode, bool msgDirection));
};

Router & Router::getInstance() {
    static MockRoute mockRoute;
    return mockRoute;
}

//////////////// END OF MOCK DEFINITION//////////////////////////
/////////////////////////////////////////////////////////////////
class TimedCommandTest : public ::testing::Test 
{
protected:

    TimedCommandTest()
    : tCommand (TimedCommand::getSingleton()) {}
    
    TimedCommand & tCommand;
};

TEST_F(TimedCommandTest, initiallyEmpty)
{
    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));

    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()), 
                 routeMsg(_)).Times(0); 
    EXPECT_EQ(tCommand.executeCommand(), 0);

    EXPECT_EQ(tCommand.getNumCmds(), 0);
}

TEST_F(TimedCommandTest, SequenceOfThreeCommandsOnTime)
{
    uint8_t cmdCode = 0x1;
    tCommand.insertCommand (&cmdCode, 1, 5000);
    EXPECT_EQ (tCommand.getNumCmds(), 1);
 
    cmdCode = 0x2; 
    tCommand.insertCommand (&cmdCode, 1, 3000);
    EXPECT_EQ (tCommand.getNumCmds(), 2);

    cmdCode = 0x3;
    tCommand.insertCommand (&cmdCode, 1, 1000);
    EXPECT_EQ (tCommand.getNumCmds(), 3);

    // Now obt is 0
    OnboardTime::getSingleton(false, 0).advanceTime(); // -->1000

    // The list has three commands.
    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(1);
    EXPECT_EQ (tCommand.executeCommand(), 1);
   
    EXPECT_EQ (tCommand.getNumCmds(), 2);
    
    OnboardTime::getSingleton(false, 0).advanceTime(); // -->2000
    
    EXPECT_EQ (tCommand.executeCommand(), 0);
    OnboardTime::getSingleton(false, 0).advanceTime(); // -->3000
    
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(1);
    EXPECT_EQ (tCommand.executeCommand(), 1);
    EXPECT_EQ (tCommand.getNumCmds(), 1);

    OnboardTime::getSingleton(false, 0).advanceTime(); // -->4000

    EXPECT_EQ (tCommand.executeCommand(), 0);
    OnboardTime::getSingleton(false, 0).advanceTime(); // -->5000
  
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(1);
    EXPECT_EQ (tCommand.executeCommand(), 1);
   
    EXPECT_EQ (tCommand.getNumCmds(), 0);

    EXPECT_EQ (tCommand.executeCommand(), 0);
    EXPECT_EQ (tCommand.getNumCmds(), 0);
}

TEST_F (TimedCommandTest, MultipleCommandsInARow)
{
    uint32_t cmdCode = 0x1;
    uint8_t * cmdArgPtr = (uint8_t *)&cmdCode;
    for (uint32_t index = 100; index > 0; --index) {
        tCommand.insertCommand (cmdArgPtr, 1, 6200 + index);
    } 

    OnboardTime::getSingleton(false, 0).advanceTime();
    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 6000);
    
    OnboardTime::getSingleton(false, 0).advanceTime();
    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 7000);

    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(100);   
    ASSERT_EQ (tCommand.getNumCmds(), 100);    
    EXPECT_EQ (tCommand.executeCommand(), 100);
    ASSERT_EQ (tCommand.getNumCmds(), 0);
}

TEST_F (TimedCommandTest, StartStop)
{
    uint32_t cmdCode = 0x1;
    uint8_t * cmdArgPtr = (uint8_t *)&cmdCode;

    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 7000);
    for (uint32_t index = 100; index > 0; --index) {
        tCommand.insertCommand (cmdArgPtr, 1, 6200 + index);
    }
    tCommand.pause();

    EXPECT_EQ (tCommand.getNumCmds(), 100);

    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(0);

    EXPECT_EQ (tCommand.executeCommand(), 0);

    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(100);

    tCommand.resume();
    EXPECT_EQ (tCommand.executeCommand(), 100);

    ASSERT_EQ (tCommand.getNumCmds(), 0);
}

TEST_F (TimedCommandTest, RemoveCommand)
{
    uint32_t cmdCode = 0x1;
    uint8_t * cmdArgPtr = (uint8_t *)&cmdCode;


    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 7000);

    for (uint32_t index = 100; index > 0; --index) {
        tCommand.insertCommand (cmdArgPtr, 1, 6200 + index);
    }

    EXPECT_EQ (tCommand.getNumCmds(), 100);

    EXPECT_TRUE (tCommand.removeCommand (6250));

    ASSERT_EQ (tCommand.getNumCmds(), 99);

    EXPECT_FALSE (tCommand.removeCommand (6250));

    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                routeMsg(_)).Times(99);

    EXPECT_EQ (tCommand.executeCommand(), 99);
    ASSERT_EQ (tCommand.getNumCmds(), 0);
}

TEST_F (TimedCommandTest, PartOfTheList)
{
    uint32_t cmdCode = 0x1;
    uint8_t * cmdArgPtr = (uint8_t *)&cmdCode;

    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 7000);

    EXPECT_EQ (tCommand.getNumCmds(), 0);
    tCommand.insertCommand (cmdArgPtr, 1, 6000); // This is an overdue command!
    for (uint32_t index = 100; index > 0; --index) {
        tCommand.insertCommand (cmdArgPtr, 1, 6950 + index); // 7050 -->6951
    }

    ASSERT_EQ (tCommand.getNumCmds(), 101);

    ON_CALL (static_cast<MockRoute &>(Router::getInstance()),
             routeMsg(_)).WillByDefault(Return(true));
    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(50); // Number of commands due

    EXPECT_EQ (tCommand.executeCommand(), 50); // Number of due commands should all
                                               // execute.
    ASSERT_EQ (tCommand.getNumCmds(), 50); // The overdue command should be deleted!

    OnboardTime::getSingleton().advanceTime();
    ASSERT_EQ (OnboardTime::getSingleton().getTime(), 8000);

    EXPECT_CALL (static_cast<MockRoute &>(Router::getInstance()),
                 routeMsg(_)).Times(50); // The remaining commands are due

    EXPECT_EQ (tCommand.executeCommand(), 50);
    ASSERT_EQ (tCommand.getNumCmds(), 0);
}
