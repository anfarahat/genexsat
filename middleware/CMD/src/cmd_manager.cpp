#include "cmd_manager.h"

// This will only be called from a derived class
// since the current class is abstract.

void * CmdMgr::executeCmd (CmdCode_t cmd, void * args)
{
    if (commandMap_.find(cmd) == commandMap_.end()) {
        // Command not supported!
        printf ("Command code %d not recognized!\n", cmd);
        return nullptr; 
    }

    return (CmdMgr::getSingleton().*commandMap_[cmd])(args);
}

