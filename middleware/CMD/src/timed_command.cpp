#include "timed_command.h"
#include "onboard_time.h"
#include "cmd_manager.h"
#include "router.h"
#include <cstdint>
#include <cassert>
#include <cstring>

#include <map> // We need a sorted container
#include <atomic>

// This enumeration has to be created in a header
// file. Note that these are place holders for
// the control commands.


struct CmdMsg
{
    CmdCode_t cmdCode;
    uint8_t * args;
    CmdMsg()
    : cmdCode (),
      args (nullptr) {}
};


class TimedCommandImpl : public TimedCommand
{
public:
    constexpr static uint64_t TOLERANCE_MS = 1000; /*One second*/
    constexpr static uint32_t MAX_NUM_TIMED_COMMANDS = 1000;
    TimedCommandImpl()
    : commandList_ {},
      enable (true) {}

    uint32_t executeCommand() override;
    void insertCommand (const uint8_t * cmdMsg, const uint32_t len, const int64_t onboardTime) override;

    bool removeCommand (const int64_t onboardTime) override;

    void pause() override;
    void resume() override;
    uint32_t getNumCmds() override;
private:
    // STL implements this as a binary tree
    // Note that we need a totally ordered container to be able
    // to retreive its top entry.
    std::map <int64_t, CmdMsg> commandList_;
    std::atomic<bool> enable;
};


uint32_t TimedCommandImpl::getNumCmds()
{
    return commandList_.size();
}

uint32_t TimedCommandImpl::executeCommand()
{
    uint32_t numOfExecCmds = 0;
    InternalMsg tTCmdFrame;
    uint32_t tTCmdHeader;
    
    if (enable) {
        // We rely on STL to find us
        // the command due.
        // Every command  will be checked every
        // possible instant. Thus we are quite
        // sure we should not miss any stored
        // command, unless the thread is pretty slow.
        // In this case, the app should restart.
        for (std::map<int64_t, CmdMsg>::iterator itTop = commandList_.begin();
             itTop != commandList_.end(); ++itTop) {
            int64_t onboardTime = OnboardTime::getSingleton().getTime();
            

            if (itTop->first <= onboardTime) {// Top of the list was actually due!
            
                // Execute if not Past-Due
                if (itTop->first > onboardTime - TOLERANCE_MS) {
                    tTCmdHeader = (itTop->second).cmdCode;
                    // Build Internal Message,
                    // Something is wrong!
                    tTCmdFrame.header.setHeader(tTCmdHeader, (tTCmdHeader >> 24));
                    
                    if ((itTop->second).args != nullptr) {
                        memcpy(tTCmdFrame.body.data, (itTop->second).args, tTCmdFrame.header.length);
                    }

                    Router & router = Router::getInstance();
                    if (router.routeMsg(tTCmdFrame)) {
                        numOfExecCmds++;
                    }
                }
                // After execution, delete args
                if (itTop->second.args != nullptr) {
                    delete [] itTop->second.args;
                }
                // Erase entry
                commandList_.erase(itTop->first);
            } else {
                break;
            }
        }
    }

    return numOfExecCmds;
}

void TimedCommandImpl::insertCommand(const uint8_t * timedCommand, const uint32_t len, const int64_t onboardTime)
{
    CmdMsg timedCmd;

    assert (len > 0);
    timedCmd.args = nullptr;

    if (len > 1) {
        timedCmd.args = new uint8_t [len -1]; // has to be deleted after execution.

        memcpy (timedCmd.args, &timedCommand[1], len - 1); // We know the number of arguments from the command code.
                                              // Each function should parse the command accordingly.
    }

    timedCmd.cmdCode = static_cast<CmdCode_t>(timedCommand[0]);

    // Now insert:
    commandList_ [onboardTime] = timedCmd;
}

bool TimedCommandImpl::removeCommand (const int64_t onboardTime)
{
    bool ret = false;

    std::map <int64_t, CmdMsg>::iterator it = commandList_.find (onboardTime);

    if (it != commandList_.end()) {
        if (it->second.args != nullptr) {
            delete [] it->second.args;
        }
        if (commandList_.erase(it->first) != 0) {
            ret = true;
        }
    }

    return ret;
}


void TimedCommandImpl::pause ()
{
    enable = false;
}

void TimedCommandImpl::resume()
{
    enable = true;
}

TimedCommand & TimedCommand::getSingleton()
{
    static TimedCommandImpl timedCommandS;
    return timedCommandS;
}
