#pragma once
#include <cstdint>

//////////////////////////////////////////////////
/// file: timed_command.h
///
/// Description: Interface for stored command
///              execution engine.
///              Uses onboard time component 
///              to synchronize command execution.
//////////////////////////////////////////////////

class TimedCommand
{
public:
    virtual ~TimedCommand(){};
    
    // Returns the number  of timed command executed on time,
    // Assumption is that this method
    // will execute periodically as a timer
    // event callback.
    virtual uint32_t executeCommand() = 0;
    // cmdMsg: Raw Timed Command Message
    // Len: Number of bytes in the message, including the command code
    virtual void insertCommand (const uint8_t *cmdMsg, const uint32_t len, const int64_t onboardTime) = 0;

    // Removes the command corresponding to the given
    // onboard time.
    virtual bool removeCommand (const int64_t onboardTime) = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual uint32_t getNumCmds() = 0;

    static TimedCommand & getSingleton();    
};



