#pragma once
#include <unordered_map>
#include <cstdint>

typedef uint32_t CmdCode_t;

class CmdMgr
{
public:
    virtual ~CmdMgr(){}

    virtual void * executeCmd(CmdCode_t cmd, void * args);
    
    // Factory
    static CmdMgr & getSingleton();
   
    // Polymorphic initialization of the map
    virtual void initMap() = 0;
   
protected:
    ::std::unordered_map<CmdCode_t, void * (CmdMgr::*)(void * args)> commandMap_;
};


