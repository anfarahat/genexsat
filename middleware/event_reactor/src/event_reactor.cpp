#include "event_reactor.h"
#include "narss_reactive_object.h"
#include <poll.h>
#include <unordered_map> // For registery
#include <memory> // for unique pointers
#include <unistd.h>
typedef int32_t EventDescriptor;

class EventReactorImpl : public EventReactor
{
public:  
    // Implementing Interface
    bool registerCallback (ReactiveObject & Object, 
                           void * (*callback)(void *),
                           void * args) override;

    bool runEventLoop() override;

private:
    struct CallbackData {
        void * (*callback)(void *);
        void * args;
    };
    std::unordered_map <ReactiveObject *, void *(*)(void *)> registryCb;
    std::unordered_map <ReactiveObject *, void *> registryArgs;
}; 

bool EventReactorImpl::registerCallback (ReactiveObject & Object, 
                                         void * (*callback)(void *),
                                         void * args)
{
   bool entryIsNew = false;
   
   // If entry not found in the registery
   if (registryCb.find(&Object) == registryCb.end()) {
       registryCb [&Object] = callback;
       registryArgs [&Object] = args;
       entryIsNew = true;
   } else { // Entry exists
       entryIsNew = false;
   }
   
   return entryIsNew; 
}
// Assumption, we call this function
// only after registering all the input
// events prior to their occurence.
bool EventReactorImpl::runEventLoop()
{
    // Code needs to be refactored for readability
    // Put into private functions
    try {
        // setup the poll structures
        std::unique_ptr<struct pollfd[]> fileDescriptorsUnique(new struct pollfd [registryCb.size()]);
        struct pollfd * fileDescriptors = fileDescriptorsUnique.get();

        // Initialize the descriptors based on the registery
        // We have to do it here to make sure we are
        // done registering all fileDescriptors.
        uint32_t index = 0;
        for (std::unordered_map<ReactiveObject *, void * (*)(void *)>::iterator it = registryCb.begin(); 
             it != registryCb.end(); 
             ++it) {
            fileDescriptors [index].fd = it->first->getDescriptor(); // iterate through unordered map
            fileDescriptors [index++].events = POLLIN;
        }
        for (;;) {
            int32_t numOfEvents = poll (fileDescriptors, 
                                        registryCb.size(), -1); // for indefinite wait
            // poll here on all events
            if (numOfEvents < 0) {
                throw 5;
            }
            // Check which events happended and callback their corresponding functions;
            index = 0;
            for (std::unordered_map<ReactiveObject *, void * (*)(void *)>::iterator it = registryCb.begin();
                 it != registryCb.end(); ++it) {
                if (fileDescriptors [index].revents & POLLIN) {
                    // Call the callback here!
                    // if timer type, read file descriptor. This is necessary
                    // so that the next poll blocks on the fd. Otherwise,
                    // poll will not block.
                    //size_t s;
                    //read(fileDescriptors [index].fd, &s, sizeof(s));
                    //printf ("timer read = %d", s);
                    it->first->defaultCallback(nullptr); // needed to handle special behavior
                                                  // for particular ReactiveObjects
                    (registryCb [it->first])(registryArgs[it->first]);
                }
                if (fileDescriptors [index].revents & POLLERR) {
                    throw 6; // error occurred
                }
                if (fileDescriptors [index++].revents & POLLNVAL) {
                    throw 7; // invalid file descriptor
                }
            }
            
        }
    }
    catch (int e) {
        switch (e) {
        case 5:
            printf ("Error Polling!\n");
        break;
        case 6:
            printf ("Error event occurred!\n");
        break;
        case 7:
            printf ("Invalid file descriptor!\n");
        break;
        default:
        break;
        }
        printf ("exception happened with code %d\n", e);
        return false;
    }
    catch ( ... ) {
        return false;
    }
    // This should never return.
    // Block to react to events
    // This is where all the magic happens!
    
}


/////////////////////////////////////////
/// Singleton: We have only one event reactor
///            per application.
/////////////////////////////////////////
EventReactor & EventReactor::getSingleton()
{
    static EventReactorImpl eventReactorS;
    return eventReactorS;
}

