#pragma once


enum Event
{
    EXT_MSG_SERIAL = 0, // Message on serial port
    INT_MSG_QUE,        // Message in message queue
    TIMER_EXPIRED,      // A timer expired
    MAX_NUM_EVENTS
};

class ReactiveObject;

class EventReactor
{
public:
    virtual ~EventReactor(){}  
    // Register callback function
    virtual bool registerCallback (ReactiveObject  & Object, 
                                   void * (*callback)(void*),
                                   void * args = nullptr) = 0;
    // Run Reactor
    virtual bool runEventLoop() = 0;

    static EventReactor & getSingleton();
};
