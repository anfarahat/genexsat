#include "tlm_controller.h"
#include "tlm_sampler.h"
#include "config_file_handler.h"

using namespace std;

class TlmControllerImpl : public TlmController
{
public:

    struct Configuration {
        // Specify the element
        // of the configuration here
        // Might consist of a map from sensor
        // type to sampling rate
	
        TlmMode mode;
    };

    static constexpr uint8_t DEFAULT_M = 1;
    static constexpr uint8_t DEFAULT_N = 60;

    TlmControllerImpl (uint32_t totalNumTlmFrames, uint32_t defaultM, uint32_t defaultN)
    {
        ConfigureParameter::getSingleton().getConfigParameter(TLM_CONTROLLER_MODE, (uint32_t &) mode_);

        for(uint32_t i=0; i<totalNumTlmFrames; i++)
        {
            measurementProgram[TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeArrayPtr[i]].NumOfMeasurements = defaultM;
            measurementProgram[TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeArrayPtr[i]].NumOfSeconds = defaultN;
        }
    }

    void setConfiguration (uint8_t const * inputConfig) override;
    void getConfiguration (uint8_t const * outputConfig) override;
    void insertConfiguration (uint32_t tlmFrameCode, uint32_t M, uint32_t N) override;
    void setMode(TlmMode mode) override;
    TlmMode getMode() override;
    void setRateForAllFrames(uint16_t samplingRate) override;

private:
    TlmMode mode_;

};


void TlmControllerImpl::setConfiguration (uint8_t const * inputConfiguration)
{
}

void TlmControllerImpl::getConfiguration (uint8_t const * outputConfiguration)
{
}

void TlmControllerImpl::insertConfiguration (uint32_t tlmFrameCode, uint32_t M, uint32_t N)
{
    unordered_map<TlmFrameCode, MeasurementPair>:: iterator it;
    
    it = measurementProgram.find(tlmFrameCode);
    
    if((it != measurementProgram.end()) && (M < N))
    {
        measurementProgram [tlmFrameCode] = {M, N};
    }
}

void TlmControllerImpl::setMode(TlmMode mode)
{
    mode_ = mode;

    ConfigureParameter::getSingleton().recordConfigParameter(TLM_CONTROLLER_MODE, mode_);
}

TlmController::TlmMode TlmControllerImpl::getMode()
{
    return mode_;
}

void TlmControllerImpl::setRateForAllFrames(uint16_t samplingRate)
{
    unordered_map<TlmFrameCode, MeasurementPair>:: iterator it;

    for(it=measurementProgram.begin(); it!=measurementProgram.end(); it++)
    {
        (*it).second = {DEFAULT_M,samplingRate};
    }
}

TlmController & TlmController::getSingleton ()
{
    static TlmControllerImpl tlmControllerS(TlmSampler::getSingleton().tlmParametersInstance.noTlmFrames, TlmControllerImpl::DEFAULT_M, TlmControllerImpl::DEFAULT_N);
    return tlmControllerS; 
}
