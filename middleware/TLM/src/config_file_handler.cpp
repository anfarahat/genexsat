#include <fstream>
#include <iostream>
#include "config_file_handler.h"


using namespace std;

class ConfigureParameterImpl : public ConfigureParameter
{
public:
    
    void recordConfigParameter(ConfigParameter configurationParameter, uint32_t parameterValue) override;
    void getConfigParameter(ConfigParameter configurationParameter, uint32_t & parameterValue) override;

    ConfigureParameterImpl();
    ~ConfigureParameterImpl();

private:
    const std::string fileName = "central_configuration.bin";
    fstream configurationFile;    
};


ConfigureParameterImpl::ConfigureParameterImpl() 
: configurationFile("central_configuration.bin", ios::binary | ios::in | ios::out )
{
}

ConfigureParameterImpl::~ConfigureParameterImpl()
{
    configurationFile.close();
}

void ConfigureParameterImpl::recordConfigParameter(ConfigParameter configurationParameter, uint32_t parameterValue)
{
    uint32_t writeIndex = 0;

    map<ConfigParameter, ConfigParameterIdentification>:: iterator it;
    
    it = configurationParametersMap.find(configurationParameter);

    if(it != configurationParametersMap.end())
    {
        writeIndex = (*it).second.ConfigParameterFileIndex;
        configurationFile.seekp(writeIndex);

        for(uint8_t i=0; i<(*it).second.ConfigParameterLen; i++)
        {
            configurationFile.put(static_cast<uint8_t>(parameterValue>>(i*8)));
        } 
        configurationFile.flush();
    }    
}

void ConfigureParameterImpl::getConfigParameter(ConfigParameter configurationParameter, uint32_t & parameterValue)
{
    uint32_t readIndex = 0;
    
    parameterValue = 0;

    map<ConfigParameter, ConfigParameterIdentification>:: iterator it;
    
    it = configurationParametersMap.find(configurationParameter);

    if(it != configurationParametersMap.end())
    {
        readIndex = (*it).second.ConfigParameterFileIndex;
        configurationFile.seekg(readIndex);

        for(uint8_t i=0; i<(*it).second.ConfigParameterLen; i++)
        {
            char tmp;
            
            if(configurationFile.get(tmp))
            {
                uint32_t readVal = 0;
                
                readVal = 0XFF & (int)tmp; 

//                cout << '\t' << std::hex << " (int)readVal = " <<(int)readVal << endl; 
             
               parameterValue = parameterValue | (readVal << (8*i));  
            }        
            else
            {
                cout << '\t' << " Print Configuration File Status bits" << endl; 
                cout << '\t' << " good()=" << configurationFile.good() << endl;
                cout << '\t' << " eof()=" << configurationFile.eof() << endl;
                cout << '\t' << " fail()=" << configurationFile.fail() << endl;
                cout << '\t' << " bad()=" << configurationFile.bad() << endl;
            }
        } 
    } 
//    cout << '\t' << std::hex << " parameterValue = " <<(int)parameterValue << endl; 
}

// First call will instatiate.
// Access always through public interface
ConfigureParameter & ConfigureParameter::getSingleton()
{
    static ConfigureParameterImpl configureParameterS;

    return configureParameterS;
}

