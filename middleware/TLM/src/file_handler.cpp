#include <fstream>
#include "file_handler.h"
#include "tlm_sampler.h"
#include "config_file_handler.h"

#include <iostream>

using namespace std;

class FileHandlerImpl : public FileHandler
{
public:
    
    void writeFrame(int8_t *tlmFrameBuf) override;
    uint8_t readFrame(int8_t *tlmFrameBuf) override;
    FileHandlerImpl();
    ~FileHandlerImpl();

private:
    const std::string fileName = "tlm_recorded_frames.bin";
    const uint32_t TLM_RECORDED_FILE_SIZE = TlmSampler::getSingleton().tlmParametersInstance.recordedTlmFileSize; //100;
    uint32_t writeIndex = 0;
    uint32_t readIndex = 0;
    uint32_t remainingNoOfFrames = 0;
    uint8_t firstRun = true;
    fstream tlmRecordedFile;

    void readBytesFromFile(int8_t *tlmFrameBuf, uint8_t noBytes);
    void writeBytesToFile(int8_t *tlmFrameBuf, uint8_t noBytes);
    
};

FileHandlerImpl::FileHandlerImpl() 
: tlmRecordedFile("tlm_recorded_frames.bin", ios::binary | ios::in | ios::out),
  firstRun(true)
{
    ConfigureParameter::getSingleton().getConfigParameter(TLM_FILE_WRITE_INDEX, writeIndex);
//    cout << endl;
//    cout << '\t' << "writeIndex = " <<  writeIndex << endl;
    
    ConfigureParameter::getSingleton().getConfigParameter(TLM_FILE_READ_INDEX, readIndex);
//    cout << endl;
//    cout << '\t' << "readIndex = " <<  readIndex << endl;
}

FileHandlerImpl::~FileHandlerImpl()
{
    tlmRecordedFile.close();
}

void FileHandlerImpl::writeBytesToFile  (int8_t *tlmFrameBuf, uint8_t noBytes)
{
    tlmRecordedFile.seekp(writeIndex);

    for(uint8_t i=0; i<noBytes; i++)
    {
        tlmRecordedFile.put(tlmFrameBuf[i]);
        writeIndex++;
        if (writeIndex == TLM_RECORDED_FILE_SIZE)
        {
            writeIndex = 0;
            tlmRecordedFile.seekp(0);
        }
    } 
 //   writeIndex = 0xFFEEDDCC;
    ConfigureParameter::getSingleton().recordConfigParameter(TLM_FILE_WRITE_INDEX, writeIndex);
 //   cout << endl;
 //   cout << '\t' << "writeIndex = " <<  writeIndex << endl;
 //   ConfigureParameter::getSingleton().getConfigParameter(TLM_FILE_WRITE_INDEX, readIndex);
}

void FileHandlerImpl::writeFrame(int8_t *tlmFrameBuf)
{    
    if(tlmRecordedFile.is_open())
    {
        uint8_t frameLen = 0;
        uint8_t frameLenRemainder = 0;
        uint8_t reqReadBytes = 0;

        frameLen = tlmFrameBuf[TlmSampler::getSingleton().tlmParametersInstance.tlmPayloadLenIndex] + TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeLen + sizeof(uint64_t);  // 4 bytes: header, 8 bytes: timestamp

        if(firstRun)
        {
            firstRun = false;
        }
        else
        {
            if( (writeIndex < readIndex) && ((writeIndex+frameLen) > readIndex) )
            {
                reqReadBytes = writeIndex + frameLen - readIndex;
            }
            else if ((writeIndex > readIndex) && ((writeIndex+frameLen) > TLM_RECORDED_FILE_SIZE) && ((writeIndex+frameLen-TLM_RECORDED_FILE_SIZE) > readIndex))
            {
                reqReadBytes = writeIndex + frameLen - TLM_RECORDED_FILE_SIZE - readIndex;
            }

            if(reqReadBytes)
            {
                uint8_t readBytes = 0;
                while(readBytes < reqReadBytes)
                {
                    int8_t tempTlmFrameBuf[TlmSampler::getSingleton().tlmParametersInstance.maxTlmPayloadLen];
                    readBytes += readFrame(tempTlmFrameBuf);
                }
            }
        }

        //write the frame
        writeBytesToFile(tlmFrameBuf, frameLen);
        remainingNoOfFrames++;
        tlmRecordedFile.flush();
    }    
}


void FileHandlerImpl::readBytesFromFile(int8_t *tlmFrameBuf, uint8_t noBytes)
{
    uint8_t success_read_flag = 0;

    tlmRecordedFile.seekg(readIndex);

    for(uint8_t i=0; i<noBytes; i++)
    {
        char tmp;
        if(tlmRecordedFile.get(tmp))
        {
            success_read_flag = 1;
            tlmFrameBuf[i] = tmp;
            readIndex++; 
            if (readIndex == TLM_RECORDED_FILE_SIZE)
            {
                readIndex = 0;
                tlmRecordedFile.seekg(0);   
            }  
        }        
        else
        {
            success_read_flag = 0;
            cout << '\t' << " Print File Status bits" << endl; 
            cout << '\t' << " good()=" << tlmRecordedFile.good() << endl;
            cout << '\t' << " eof()=" << tlmRecordedFile.eof() << endl;
            cout << '\t' << " fail()=" << tlmRecordedFile.fail() << endl;
            cout << '\t' << " bad()=" << tlmRecordedFile.bad() << endl;
        }
    }

    if (success_read_flag)
    {
        ConfigureParameter::getSingleton().recordConfigParameter(TLM_FILE_READ_INDEX, readIndex);
//        cout << endl;
//        cout << '\t' << "readIndex = " <<  readIndex << endl;
    }
}


uint8_t FileHandlerImpl::readFrame(int8_t *tlmFrameBuf)
{
    uint8_t noReadBytes = 0;

    if(tlmRecordedFile.is_open())
    {
        if(!firstRun)
        {
            if(remainingNoOfFrames)
            {
                //read the frame header
                readBytesFromFile(tlmFrameBuf, TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeLen);

                //caculate the frame size
                uint8_t frameSize = tlmFrameBuf[TlmSampler::getSingleton().tlmParametersInstance.tlmPayloadLenIndex] + sizeof(uint64_t);

                //read the frame
                readBytesFromFile(&tlmFrameBuf[TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeLen], frameSize);

                noReadBytes = tlmFrameBuf[TlmSampler::getSingleton().tlmParametersInstance.tlmPayloadLenIndex] + TlmSampler::getSingleton().tlmParametersInstance.tlmFrameCodeLen + sizeof(uint64_t);  // 4 bytes: header, 8 bytes: timestamp
                
                remainingNoOfFrames--;
            }
        }
    }
    
    return noReadBytes;
}


// First call will instatiate.
// Access always through public interface
FileHandler & FileHandler::getSingleton()
{
    static FileHandlerImpl fileHandlerS;

    return fileHandlerS;
}

