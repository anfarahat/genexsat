#include "tlm_sampler.h"
#include "onboard_time.h"
#include "narss_msg_xmit.h"
#include "file_handler.h"

#include <cstring>


using namespace std;

class TlmSamplerImpl : public TlmSampler
{
private:
    uint32_t currNumOfSec;
    void constructEmptyFrame(int8_t *tlmFrameBuf);
    void insertOBCTimeIntoBuf(int8_t *timestampPosition);
  
public:
    TlmSamplerImpl();
	
    void requestTlm(uint8_t *mapIndex) override;

    void rxAndRecordFrame(uint32_t rxTlmFrameCode, int8_t *tlmRespBuf) override;
	
    void playback(int8_t * tlmWindowBuf) override;

    void directTransmission(int8_t * tlmDirectTransmissionBuf) override;

    void getSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf) override;

    void internalGetSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf) override;

};

TlmSamplerImpl::TlmSamplerImpl()
: currNumOfSec(0)
{
}

TlmSampler & TlmSampler::getSingleton()
{
    static TlmSamplerImpl tlmSamplerS;
    return tlmSamplerS;
}

void TlmSamplerImpl::requestTlm(uint8_t * mapIndex)
{
    uint32_t i=0;
    currNumOfSec++;
    
    unordered_map<TlmController::TlmFrameCode, TlmController::MeasurementPair>:: iterator it;
//map<TlmController::TlmFrameCode, TlmController::MeasurementPair>:: iterator it;

    for(it=TlmController::getSingleton().measurementProgram.begin(); it!=TlmController::getSingleton().measurementProgram.end(); it++)
    {
        uint32_t samplerIndicator = currNumOfSec % (*it).second.NumOfSeconds;
        if(samplerIndicator < (*it).second.NumOfMeasurements)
        {
            mapIndex[i] = true;
        }
        else
        {
            mapIndex[i] = false;
        }
        i++;
    }  
}

void TlmSamplerImpl::rxAndRecordFrame(uint32_t rxTlmFrameCode, int8_t *tlmRespBuf)
{
    uint8_t tlmFrameWriteIndex = 0;
    uint8_t i = 0;

    map<TlmController::TlmFrameCode, tlmFramePtr>:: iterator it;

    it = recentTlmFrames.find(rxTlmFrameCode);
    
    if(it != recentTlmFrames.end())
    {
        // free previously allocated memory
        delete[] (*it).second;
    }

    uint8_t tlmFrameResponseLength = tlmRespBuf[tlmParametersInstance.tlmPayloadLenIndex];

    int8_t * recentTlmFrameBuf = new int8_t[tlmFrameResponseLength+(tlmParametersInstance.tlmFrameCodeLen)+sizeof(uint64_t)]; 

    for(i=0; i<(tlmFrameResponseLength+(tlmParametersInstance.tlmFrameCodeLen)); i++)
    {
        recentTlmFrameBuf[tlmFrameWriteIndex++] = tlmRespBuf[i];
    }

    // add timestamp 
    insertOBCTimeIntoBuf(&recentTlmFrameBuf[tlmFrameWriteIndex]);   
    
    recentTlmFrames[rxTlmFrameCode] = recentTlmFrameBuf;

    TlmController::TlmMode currTlmMode;
    currTlmMode = TlmController::getSingleton().getMode(); 
    if (currTlmMode == TlmController::RECORDING)
    {   
        // record in file 
        FileHandler::getSingleton().writeFrame(recentTlmFrameBuf);
    }  
}

void TlmSamplerImpl::insertOBCTimeIntoBuf(int8_t *timestampPosition)
{
    uint64_t timestamp = 0;
    uint8_t i = 0;

    timestamp = OnboardTime::getSingleton().getTime();

    for(i=0; i<sizeof(uint64_t); i++)
    {
        timestampPosition[i] = (uint8_t) (timestamp >> (i*8));
    } 
}

void TlmSamplerImpl::constructEmptyFrame(int8_t *tlmFrameBuf)
{
    uint8_t tlmEmptyFrameWriteIndex = 0;
    uint64_t emptyFrameTimestamp = 0;

    tlmFrameBuf[tlmEmptyFrameWriteIndex++] = (int8_t) (tlmParametersInstance.tlmEmptyFrameCode);
    tlmFrameBuf[tlmEmptyFrameWriteIndex++] = (int8_t) (tlmParametersInstance.tlmEmptyFrameCode >> 8);
    tlmFrameBuf[tlmEmptyFrameWriteIndex++] = (int8_t) (tlmParametersInstance.tlmEmptyFrameCode >> 16);
    tlmFrameBuf[tlmEmptyFrameWriteIndex++] = (int8_t) (tlmParametersInstance.tlmEmptyFrameCode >> 24);

    // add timestamp 
    insertOBCTimeIntoBuf(&tlmFrameBuf[tlmEmptyFrameWriteIndex]);     
}

void TlmSamplerImpl::playback(int8_t * tlmWindowBuf)
{
    uint32_t tlmWindowBufWriteIndex = 0;
    uint8_t noReadBytes = 0;
    uint8_t i = 0;

    for(i=0; i< tlmParametersInstance.windowSize; i++)
    {
        // read frame from file
        int8_t tlmFrameBuf[tlmParametersInstance.maxTlmPayloadLen];
        noReadBytes = FileHandler::getSingleton().readFrame(tlmFrameBuf);
            
        if(noReadBytes)
        {
            memcpy(&tlmWindowBuf[tlmWindowBufWriteIndex], tlmFrameBuf, noReadBytes);
            tlmWindowBufWriteIndex = tlmWindowBufWriteIndex + noReadBytes;
        }
        else
        {
            // construct empty frame
            constructEmptyFrame(tlmFrameBuf);
            memcpy(&tlmWindowBuf[tlmWindowBufWriteIndex], tlmFrameBuf, (tlmParametersInstance.tlmFrameCodeLen)+sizeof(uint64_t));
            return;
        }
    }
}

void TlmSamplerImpl::directTransmission(int8_t * tlmDirectTransmissionBuf)
{
    uint32_t tlmDirectTransmissionBufWriteIndex = 0;

    map<TlmController::TlmFrameCode, tlmFramePtr>:: iterator it;

    for(it=recentTlmFrames.begin(); it!=recentTlmFrames.end(); it++)
    {
        memcpy(&tlmDirectTransmissionBuf[tlmDirectTransmissionBufWriteIndex], (*it).second, ((*it).second[3] + (tlmParametersInstance.tlmFrameCodeLen) + sizeof(uint64_t)));

        tlmDirectTransmissionBufWriteIndex = tlmDirectTransmissionBufWriteIndex + ((*it).second[3] + (tlmParametersInstance.tlmFrameCodeLen) + sizeof(uint64_t));      
    }
}

void TlmSamplerImpl::getSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf)
{
    map<TlmController::TlmFrameCode, tlmFramePtr>:: iterator it;

    it = recentTlmFrames.find(requestedTlmFrameCode);
    
    if(it == recentTlmFrames.end())
    {
        // construct empty frame
        constructEmptyFrame(requestedTlmBuf);
    }
    else
    {
        requestedTlmBuf = (*it).second;
    }
}

void TlmSamplerImpl::internalGetSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf)
{
    map<TlmController::TlmFrameCode, tlmFramePtr>:: iterator it;

    it = recentTlmFrames.find(requestedTlmFrameCode);
    
    if(it == recentTlmFrames.end())
    {
        // construct empty frame
        constructEmptyFrame(requestedTlmBuf);
    }
    else
    {
        requestedTlmBuf = (*it).second;
    }
}



