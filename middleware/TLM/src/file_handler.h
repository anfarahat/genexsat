#pragma once

//#include <fstream>


class FileHandler
{
public:
    static FileHandler & getSingleton();

    virtual void writeFrame(int8_t *tlmFrameBuf) = 0;
    virtual uint8_t readFrame(int8_t *tlmFrameBuf) = 0;
};

