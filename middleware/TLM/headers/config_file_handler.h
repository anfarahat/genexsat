#pragma once

#include <cstdint>
#include <map>

enum ConfigParameter
{
    TLM_FILE_WRITE_INDEX = 0,
    TLM_FILE_READ_INDEX,
    TLM_CONTROLLER_MODE,
    
    MAX_NUM_OF_CONFIG_PARAMETER
};


class ConfigureParameter
{
public:

    struct ConfigParameterIdentification 
    {
        uint32_t ConfigParameterFileIndex; 
        uint32_t ConfigParameterLen; 
    };

    std::map<ConfigParameter, ConfigParameterIdentification> configurationParametersMap;

    virtual void recordConfigParameter(ConfigParameter configurationParameter, uint32_t parameterValue) = 0;
    
    virtual void getConfigParameter(ConfigParameter configurationParameter, uint32_t & parameterValue) = 0;

    static ConfigureParameter & getSingleton();
};
