#pragma once
#include <cstdint>
#include <unordered_map>
//#include <map>


class TlmController
{
public:
    enum TlmMode
    {
        IDLE = 0,
        RECORDING,
        PLAYBACK,
        DIRECT_TRANSMISSION,
        MAX_NUM_TLM_MODES
    };

    struct MeasurementPair 
    {
        uint32_t NumOfMeasurements; //M
        uint32_t NumOfSeconds; //N
    };

    typedef uint32_t TlmFrameCode;

    std::unordered_map<TlmFrameCode, MeasurementPair> measurementProgram;
//map<TlmFrameCode, MeasurementPair> measurementProgram;
    
    virtual void setConfiguration (uint8_t const * inputConfig) = 0;
    virtual void getConfiguration (uint8_t const * outputConfig) = 0;
    virtual void insertConfiguration (uint32_t tlmFrameCode, uint32_t M, uint32_t N) = 0;
    virtual void setMode(TlmMode) = 0;
    virtual TlmMode getMode() = 0;
    virtual void setRateForAllFrames(uint16_t samplingRate) = 0;

    static TlmController & getSingleton();
};
