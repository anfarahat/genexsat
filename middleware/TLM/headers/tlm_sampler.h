#pragma once
#include <stdint.h>
#include <map>
#include "tlm_controller.h"


struct tlmParameters
{
    uint8_t windowSize;
    uint32_t tlmEmptyFrameCode;
    uint8_t tlmId;
    uint8_t tlmEmptyFrameC1;
    uint8_t tlmEmptyFrameC2;
    uint8_t noTlmFrames;
    uint32_t *tlmFrameCodeArrayPtr;
    uint8_t tlmFrameCodeLen;
    uint8_t tlmPayloadLenIndex;
    uint32_t maxTlmPayloadLen;
    uint32_t recordedTlmFileSize;
};

class TlmSampler
{
public:

    tlmParameters tlmParametersInstance;
    
    typedef int8_t* tlmFramePtr;
    std::map<TlmController::TlmFrameCode, tlmFramePtr> recentTlmFrames;

    static TlmSampler & getSingleton();

    virtual void requestTlm(uint8_t * mapIndex) = 0;

    virtual void rxAndRecordFrame(uint32_t rxTlmFrameCode, int8_t *tlmRespBuf) = 0;

    virtual void playback(int8_t * tlmWindowBuf) = 0;

    virtual void directTransmission(int8_t * tlmDirectTransmissionBuf) = 0;

    virtual void getSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf) = 0;

    virtual void internalGetSelectedFrame(uint32_t requestedTlmFrameCode, int8_t *& requestedTlmBuf) = 0;
};




