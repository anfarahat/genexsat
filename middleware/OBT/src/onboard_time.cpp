#include "onboard_time.h"

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cstring>
#include <cassert>
#include <cstdio>
class OnboardTimeImpl : public OnboardTime
{
public:
    constexpr static const uint32_t MAX_NUM_REMOTE_THRDS = 10;
    constexpr static const char *SHMEM_FILENAME = "/obt_shmem";
    constexpr static uint32_t SHMEM_SIZE = sizeof (uint32_t) + sizeof(uint64_t)
                                           + MAX_NUM_REMOTE_THRDS*sizeof(uint32_t);
    OnboardTimeImpl(bool isMaster, uint32_t tick)
    : shmem_fd (shm_open (SHMEM_FILENAME,
                          O_CREAT | O_RDWR,
                          S_IRWXU)),
      isMaster_(isMaster),
      tick_ (tick) {
        if (shmem_fd == - 1) { 
            printf ("failed to open shared memory file descriptor, terminating!\n");
            throw;
        }
        if (ftruncate (shmem_fd, SHMEM_SIZE) != 0) {
            printf("failed to truncate shared memory, terminating!\n");
            throw;
        }
        obTime_ = (int64_t *)mmap (NULL,
                                   SHMEM_SIZE,
                                   PROT_READ | PROT_WRITE,
                                   MAP_SHARED, shmem_fd, 0);
        numOfThreads_= (uint32_t *) &obTime_[1];
       
        remoteThrdPeriod_ = &numOfThreads_[1];
        // Initialize shared memory
        if (isMaster) {
            *obTime_ = 0;
            *numOfThreads_ = 0;
            for (uint32_t threadIndex = 0; threadIndex < MAX_NUM_REMOTE_THRDS; ++threadIndex) {
                remoteThrdPeriod_[threadIndex] = 0; //means that thread is not active
            }
        }
    }

    // Please do not forget
    // to add an exception handler
    virtual ~OnboardTimeImpl() {
        munmap (obTime_, SHMEM_SIZE);
        shm_unlink (SHMEM_FILENAME);
        close (shmem_fd);
    }

    int64_t getTime() override;
    void setTime(int64_t timeValue);

    void advanceTime() override;
    uint32_t & getNumOfThreads() override;

    bool isMaster() const override {
        return isMaster_;
    }

    uint32_t & getPeriodRef (uint32_t threadId) override {
        // returns a reference to shared memory
        assert (threadId < *numOfThreads_);
        return remoteThrdPeriod_[threadId];
    }

private:
    int32_t shmem_fd;
    int64_t * obTime_;
    uint32_t * numOfThreads_;
    uint32_t * remoteThrdPeriod_;
    bool isMaster_;
    uint32_t tick_;
};


int64_t OnboardTimeImpl::getTime ()
{
    return *obTime_;
}

void OnboardTimeImpl::setTime(int64_t timeValue)
{
    *obTime_ = timeValue;
}

void OnboardTimeImpl::advanceTime()
{
    if (isMaster_) {
        (*obTime_) += tick_;
    }
}

// Client should hold the returned value in a reference
// to be able to increase it.
uint32_t & OnboardTimeImpl::getNumOfThreads()
{
    return *numOfThreads_;
}

// First call will instatiate.
// Access always through public interface
OnboardTime & OnboardTime::getSingleton(bool isMaster, uint32_t tick)
{
    static OnboardTimeImpl onboardTimeComponentS(isMaster, tick);

    return onboardTimeComponentS;
}

