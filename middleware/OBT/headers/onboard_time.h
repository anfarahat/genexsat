#pragma once

#include <stdint.h>

class OnboardTime
{
public:

    virtual ~OnboardTime() {};

    virtual int64_t getTime() = 0;
    virtual void setTime (int64_t timeValue) = 0;
    virtual void advanceTime() = 0;
    virtual uint32_t & getNumOfThreads() = 0;
    virtual bool isMaster() const = 0;
    // Provides pointer access to shared memory
    // to read a remote thread period
    virtual uint32_t & getPeriodRef (uint32_t threadId) = 0;
    static OnboardTime & getSingleton(bool isMaster = false, uint32_t tick = 10); // In ms
};


