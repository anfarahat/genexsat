#pragma once
#include <cstdint>

constexpr uint32_t MAX_MSG_LEN = 0x400; //(1kB)

#pragma pack(push, 1)

struct InternalMsgHeader
{
    uint8_t ID;
    uint8_t C1;
    uint8_t C2;
    uint16_t length;
    uint8_t src;
    uint16_t const reserved;
    InternalMsgHeader() 
    : ID(0), C1(0), C2(0), length(0), src(0), reserved(0) {}

    InternalMsgHeader & operator= (const InternalMsgHeader & header) {
        this->ID = header.ID;
        this->C1 = header.C1;
        this->C2 = header.C2;
        this->length = header.length;
        this->src = header.src;

        return *this;
    }

    inline uint32_t cmdCodeNoLength() const
    {
        uint32_t ret = ((static_cast<uint32_t>(C2)     << 16) |
                        (static_cast<uint32_t>(C1)     << 8 ) | ID);
        return (ret);
    }
    
    inline uint16_t getLength() const {
        return length;
    }

    inline uint8_t getSrc() const {
        return src;
    }

    inline uint64_t cmdCodeFull() const
    {
        return *(uint64_t *)this;
    }

    inline uint32_t cmdCode() const
    {
        return *(uint32_t *)this;
    }

    inline void setHeader(const uint32_t cmdCode, uint16_t len, uint8_t src = 0)
    {
        this->ID =  cmdCode      & 0xFF;
        this->C1 = (cmdCode>>8 ) & 0xFF;
        this->C2 = (cmdCode>>16) & 0xFF;
        this->length = len;
        this->src = src;
    }
};
#pragma pack(pop)


struct InternalMsgBody
{
    uint8_t  data [MAX_MSG_LEN];
};


struct InternalMsg
{
    InternalMsgHeader header;
    InternalMsgBody   body;
};


class Router
{
public:
    static Router& getInstance();
    
    // Transmits a message based on its header
    // to the correct destination

    virtual uint16_t getFrameLength(uint32_t msgCode, bool msgDirection) =0;


    virtual bool routeMsg(InternalMsg & msg) = 0;

};
