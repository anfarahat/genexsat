# GenExSat

(c) 2018 Dr. Aly Farahat All Right Reserved
 
This software is provided without warranty or responsibility upon copyright owner(s). It is the sole responsibility of the user of this software to guarantee its adequate functionality.

This software is licensed under GNU Public License version 3 (GPLv3) and is provided free-of-charge.

Code base for a General Experimental Satellite (GenExSat)  platform command and data handling subsystem. Code assumes arm-cortex Linux-based processor for the embedded build. Code also builds for a desktop Linux computer.

The build system is cmake-based. Please install cmake version 3.0 or newer. 

Use `build_desktop.sh` for the Linux build. Please install arm-gnu-eabi toolchain for Linux to build the code for the arm-cortex. Use `./build_embedded.sh` to invoke the embedded build.

Please make sure that bzlib.h is installed on your Linux machine by running `sudo apt-get install libbz2-1.0 libbz2-dev`
