#!/bin/bash


key="$1"
case $key in
	clean)
	cd build/images/desktop
        cmake -DBUILD_TYPE=desktop ../../../
        make clean
        RETURN_VALUE=$?
        cd ../../../
	;;
        limited)
        cd build/images/desktop
        cmake -DBUILD_TYPE=limited ../../../
        make -j 12
        RETURN_VALUE=$?
        cd ../../../
        ;;
        test)
        cd build/images/desktop
        cmake -DBUILD_TYPE=limited ../../../
        make -j 12
        RETURN_VALUE=$?
        make check
        RETURN_VALUE=$(($? + ${RETURN_VALUE}))
        cd ../../../
        ;;
	*)
	cd build/images/desktop
        cmake -DBUILD_TYPE=desktop ../../../
	make -j 12
	RETURN_VALUE=$?
        cp ../startup.sh ../../output/Desktop/bin/
        cp ../stopAll.sh ../../output/Desktop/bin/
	cd ../../output/Desktop/bin
        chmod a+x *.sh
        cd ../../../..
	;;
esac
exit $RETURN_VALUE

#(cd build/images/desktop && cmake ../../.. && make && cd ../../../)

