#include "narss_gpio.h"

#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>

#define NUMBER_BYTES_TO_WRITE       2
#define GPIO_FILENAME_SIZE          35
#define GPIO_PINNUMBER_SIZE         3
#define GPIO_MAX_PINNUMBER_PER_PORT 32

#define IS_VALID_GPIO_ID(x) ((x >= 0) && (x < 128))

constexpr int portA_MaxPinNumber = 32;

constexpr int portB_MaxPinNumber = 64;

constexpr int portC_MaxPinNumber = 96;

constexpr int portD_MaxPinNumber = 128;

class GPIOImpl: public GPIO
{
private:
    /*
     * selects the port of the gpio and its id in the port
     */
    void select_GPIO_Port(int gpioId,char* gpio_Port_name,int* portId);
public:
    /* return -1/-2 for fail and 0 for success
     *  Set direction to 2 = output with initial high, 1 = output initial low,
     *  0 input */
    int gpio_set_direction(int gpio, GPIO_Direction_t dir) override;

    /* return -1 for failed operation or 0 for success operation
     * Single GPIO read */
    int gpio_read(int gpio) override;
    /* return -1 for failed operation or 0 for success operation
     * Set GPIO to val (1 = high) */
    int gpio_write(int gpio, int val) override;

};


void GPIOImpl::select_GPIO_Port(int gpioId,char* gpio_Port_name,int* portId)
{
    assert(IS_VALID_GPIO_ID(gpioId));
    *portId = gpioId % GPIO_MAX_PINNUMBER_PER_PORT;
    if(gpioId < portA_MaxPinNumber)
    {
        *gpio_Port_name='A';
    }
    else if (gpioId < portB_MaxPinNumber)
    {
        *gpio_Port_name='B';
    }
    else if (gpioId < portC_MaxPinNumber)
    {
        *gpio_Port_name='C';
    }
    else if( gpioId < portD_MaxPinNumber)
    {
        *gpio_Port_name='D';
    }
}

GPIO::GPIO()
{
    char fileName[GPIO_PINNUMBER_SIZE];
    int gpiofd,ret;

    gpiofd = open("/sys/class/gpio/export", O_WRONLY);
    if(gpiofd != -1) {
        /*
         * loop for exporting gpio pins ports A, B, and C
         * Note that port D is used for internal NAND flash
         */
        for (int gpio_index=0;gpio_index < portC_MaxPinNumber;gpio_index++ )
        {
            if (gpio_index == 9 || gpio_index == 10
               //port A9 to A10 are used for debug unit
               || (gpio_index >= 15 && gpio_index <= 21)
               //port A15 to A20 are SD card pins and A21 is used by NAND
               || (gpio_index >= 50 && gpio_index <= 63))
                //Port B19 to B32 are reserved (not real gpio pins)
            {
                continue;
            }
            sprintf(fileName, "%d", gpio_index);
            ret = write(gpiofd, fileName, strlen(fileName));
            if(ret < 0) {
                fprintf(stderr, "Export failed at %d",gpio_index);
                throw (errno);
                return;
            }
        }
        close(gpiofd);
    } else {
        // If we can't open the export file, we probably
        // dont have any gpio permissions
        perror("No GPIO Permissions");
        throw (errno);
        return;
    }
}

GPIO::~GPIO()
{
    int gpiofd, ret;
    char fileName[GPIO_PINNUMBER_SIZE];
    gpiofd = open("/sys/class/gpio/unexport", O_WRONLY);

    if (gpiofd != -1)
    {
        for (int gpio_index=0;gpio_index < portC_MaxPinNumber;gpio_index++ )
        {
            if (gpio_index == 9 || gpio_index == 10 \
               || (gpio_index >= 15 && gpio_index <= 21)
               || (gpio_index >= 50 && gpio_index <= 63))
            {
                continue;
            }
            sprintf(fileName, "%d", gpio_index);
            ret = write(gpiofd, fileName, strlen(fileName));
            if (ret < 0)
            {
                fprintf(stderr, "writing gpio %d to unexport file fail",gpio_index);
                throw(errno);
                return;
            }
        }
        close(gpiofd);
    }
    else
    {
        perror("unexport file open fail");
        throw (errno);
        return;
    }
}

int GPIOImpl::gpio_set_direction(int gpio, GPIO_Direction_t dir)
{
    int ret = 0;
    char fileName[GPIO_FILENAME_SIZE];
    int gpio_port_id;
    char gpio_port= '0';

    select_GPIO_Port(gpio,&gpio_port,&gpio_port_id);
    sprintf(fileName, "/sys/class/gpio/pio%c%d/direction", gpio_port,gpio_port_id);
    int gpiofd = open(fileName, O_WRONLY);
    if(gpiofd < 0) {
        perror("Couldn't open GPIO file");
        throw (errno);
        ret = -1;
    }

    if(dir == GPIO_DIR_OUT_INIT_HI && gpiofd){
        if (3 != write(gpiofd, "high", 4)) {
            perror("Couldn't set GPIO direction to out with initial = high");
            throw (errno);
            ret = -2;
        }
    }

    if(dir == GPIO_DIR_OUT_INIT_LO && gpiofd){
        if (3 != write(gpiofd, "out", 3)) {
            perror("Couldn't set GPIO direction to out with initial = low");
            throw (errno);
            ret = -2;
        }
    }
    else if(gpiofd) {
        if(2 != write(gpiofd, "in", 2)) {
            perror("Couldn't set GPIO direction to in");
            throw (errno);
            ret = -2;
        }
    }

    close(gpiofd);
    return ret;
}

int GPIOImpl::gpio_read(int gpio)
{
    int in = 0;
    char fileName[GPIO_FILENAME_SIZE];
    int nread, gpiofd;
    int gpio_port_id;
    char gpio_port= '0';

    select_GPIO_Port(gpio,&gpio_port,&gpio_port_id);
    sprintf(fileName, "/sys/class/gpio/pio%c%d/value", gpio_port,gpio_port_id);
    gpiofd = open(fileName, O_RDWR);
    if(gpiofd < 0) {
        fprintf(stderr, "Failed to open gpio %c%d value\n", gpio_port,gpio_port_id);
        throw (errno);
        return -1;
    }

    nread = read(gpiofd, &in, 1);
    if(nread == -1)
    {
        perror("GPIO Read failed");
        throw (errno);
        return -1;
    }

    close(gpiofd);
    return in;
}

int GPIOImpl::gpio_write(int gpio, int val)
{
    char fileName[GPIO_FILENAME_SIZE];
    int  ret, gpiofd;
    int gpio_port_id;
    char gpio_port= '0';

    select_GPIO_Port(gpio,&gpio_port,&gpio_port_id);
    sprintf(fileName, "/sys/class/gpio/pio%c%d/value", gpio_port,gpio_port_id);
    gpiofd = open(fileName, O_RDWR);
    if(gpiofd < 0)
    {
        perror("gpio pin open fail");
        throw (errno);
        return -1;
    }

    snprintf(fileName, NUMBER_BYTES_TO_WRITE, "%d", val);
    ret = write(gpiofd, fileName, NUMBER_BYTES_TO_WRITE);
    if(ret < 0) {
        perror("failed to set gpio");
        throw (errno);
        return -1;
    }

    close(gpiofd);
    if(ret != NUMBER_BYTES_TO_WRITE)
    {
        perror("number of written bytes is less than the number of required bytes to write");
        throw (errno);
        return -1;
    }

    return 0;
}

GPIO & GPIO::getSingleton()
{
    static GPIOImpl gpioS;
    return gpioS;
}
