
#include "narss_serial_port.h"

#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <string>
#include <cstring>
#include <cstdio>
#include <unistd.h>
#include <unordered_map>
#include <cassert>

#define IS_SERIAL_PORT_NAME(x) ((x==SerialPort::COM1P)||\
		(x==SerialPort::COM2P)||(x==SerialPort::COM3))
static std::unordered_map<std::string, SerialPort *> terminalsMap;

class SerialPortImpl : public SerialPort
{
public:
    SerialPortImpl(SerialPortName portName);

    virtual ~SerialPortImpl();

    bool readSerialData (uint8_t * const msg, uint32_t & len) override;

    bool writeSerialData (uint8_t * const msg, uint32_t len) override;

    bool getInitStatus () override;

    int32_t getDescriptor() override;

private:
    int setInterfaceAttribs(int speed);

    /*device file descriptor*/
    int fd_ttySerial;
    
    /*status variable*/
    bool initStatus; 
};

int32_t SerialPortImpl::getDescriptor()
{
    return fd_ttySerial;
}
/*
 *
 */
bool SerialPortImpl::getInitStatus()
{
    return this->initStatus;
}
/*
 * Arguments: (int speed) the speed of the serial port
 *
 * Return: (int) status indicator for the set serial attributes process
 *
 */
int SerialPortImpl::setInterfaceAttribs(int speed)
{
    /*instance of termios structure*/
    struct termios tty;

    /* check if the file descriptor is correct or not*/
    if (tcgetattr(fd_ttySerial, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    /* configure bus speed for input and output */
    cfsetspeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;    /*Clear Mask for size bit in the control Mode*/
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(PARMRK | ISTRIP | IGNBRK | BRKINT | IGNCR | ICRNL | INLCR | IXON);/*INPCK | IXOFF*/

    tty.c_lflag &= ~(ICANON | ECHO | ECHONL | ISIG | IEXTEN);/*NOFLSH*/

    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;/*fetch minimum of 1 byte*/
    tty.c_cc[VTIME] = 1;/*wait for 100ms*/

    /* set serial attributes */
    if (tcsetattr(fd_ttySerial, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
    return -1;
    }

    return 0;
}

/*
 * Arguments: (const char* portName) the path of the serial port
 *
 */
SerialPortImpl::SerialPortImpl(SerialPortName portName)
{
    char portNameSys[15];

    switch(portName)
    {
    case COM1P:
        strcpy(portNameSys,"/dev/pts/19");
    break;
    case COM2P:
        strcpy(portNameSys,"/dev/pts/20");
    break;
    case COM3:
        strcpy(portNameSys,"/dev/ttyS0");
    break;
    default:
        initStatus = false;
        return;
    }
 
    /* open the serial device for Read\Write */
    fd_ttySerial = open(portNameSys, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd_ttySerial < 0) {
        throw (errno);
        printf("Error opening %s: %s\n", portNameSys, strerror(errno));
        initStatus = false;
        return;
    }

    /* set attributes baudrate 115200 */
    if (setInterfaceAttribs(B115200) != 0) {
        throw "Bad Descriptor Problem";
        initStatus = false;
    return;
    } else {
        initStatus = true;
    }
}

/*
 * Destructor
 *
 * Close the device file descriptor
 * and
 * clear the initStatus
 */
SerialPortImpl::~SerialPortImpl()
{
    initStatus =false;
    close(fd_ttySerial);
}


/*
 * Arguments: (uint8_t* msg) pointer to the data store variable
 * 			  (uint32_t &) length of read data returned from read function
 *
 * Return: (boolean) status indicator for the read data process
 * 					 returns true for successful operation
 * 					 and false for failed operation
 *
 */
bool SerialPortImpl::readSerialData (uint8_t * const msg, uint32_t & len)
{
    int rdlen;
    /* simple read input from the serial */
    rdlen = read(fd_ttySerial, msg, len);
    /* if there is no new rx data */
    if (rdlen <= 0) {
        throw (errno);
        printf("Error from read: %d: %s\n", rdlen, strerror(errno));
        len = 0;
        return false;
    } else/* New data is available in msg buffer ->successful rx*/ {
        len = rdlen;
        return true;
    }
}

/*
 * Arguments: (uint8_t* msg) pointer to the data
 * 			  (uint32_t) length of data to write
 *
 * Return: (boolean) status indicator for the write data process
 * 					 returns true for successful operation
 * 					 and false for failed operation
 */
bool SerialPortImpl::writeSerialData (uint8_t * const msg, uint32_t len)
{
    int wrlen;
    /* simple write data to the serial */
    wrlen = write(fd_ttySerial, msg, len);
    /* if serial doesn't send any byte */
    if (wrlen <= 0) {
        throw (errno);
        printf("Error from write: %d: %s\n", wrlen, strerror(errno));
        return false;
    }
    /*if the number of sent bytes is less than the required*/
    else if ((uint32_t)wrlen != len) {
        printf("The written bytes number doesn't equal the required");
        return false;
    } else {/* required data is sent successfully*/
        return true;
    }
}

/*
 *
 * Arguments: (SerialPortName): element of enum contains the serial ports names
 *
 * Return: (SerialPortImpl &):return the pointer of the instance of SerialPortImpl
 */
SerialPort & SerialPort::getInstance(SerialPortName portName)
{
    char portNameSys[15];

    switch(portName) {
    case COM1P:
        strcpy(portNameSys,"/dev/pts/19");
    break;
    case COM2P:
        strcpy(portNameSys,"/dev/pts/20");
    break;
    case COM3:
        strcpy(portNameSys,"/dev/ttyS0");
    break;
    default:
        assert(IS_SERIAL_PORT_NAME(portName));
    break;
    }

    // Check if the serial port does not exist
    if (terminalsMap.find(portNameSys) == terminalsMap.end()) {
        terminalsMap[portNameSys] = new SerialPortImpl(portName);
    }

    return *terminalsMap[portNameSys];
}

/*
 * Arguments: (string) the name of the queue
 *
 * Return: (boolean):return the status if entry found or not
 *
 */
bool SerialPort::removeInstance (const std::string queueName)
{
    bool entryFound = true;

    if (terminalsMap.find (queueName) == terminalsMap.end()) {
    entryFound = false;
    } else {
        delete terminalsMap[queueName];
        terminalsMap.erase(queueName);
    }

    return entryFound;
}
