#include "narss_msg_queue.h"
#include "narss_reactive_object.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <semaphore.h>

#include <cstdio>
#include <cassert>
#include <string>
#include <unordered_map>

class MsgQueueImpl : public virtual MsgQueue
{
public: 
    MsgQueueImpl(const std::string queueName,
                 const uint32_t msgSize = MAX_MSG_SIZE,
                 const uint32_t maxNumMsgs = MAX_NUM_MSG);
    virtual ~MsgQueueImpl();

    bool rcvMsg  (void * const msg, uint32_t &len) override;
    bool sendMsg (void const * msg, uint32_t len,
                  MsgPriority Prio) override;
    uint32_t getNumMsgs() override;
    std::string getName() override;
    int32_t getDescriptor() override;

private:
    const uint32_t msgSize_;
    const uint32_t maxNumMsgs_;
    mqd_t qd_;
    sem_t * qSemaphore_;

    std::string name_;
    static std::unordered_map<std::string, MsgQueue &> queueRegistery;
};


MsgQueueImpl::MsgQueueImpl(const std::string queueName,
                           const uint32_t msgSize, 
                           const uint32_t maxNumMsgs)
: msgSize_(msgSize),
  maxNumMsgs_(maxNumMsgs),
  name_("/" + queueName)
             
{
    struct mq_attr attr;
    attr.mq_maxmsg  = maxNumMsgs;
    attr.mq_msgsize = msgSize;

    qd_ = mq_open (name_.c_str(), 
                  O_RDWR | O_CREAT | O_NONBLOCK,
                  S_IRWXU, &attr);

    if (qd_ == -1) {
        printf("Failed to open message queue! terminating!\n");
        throw;
    }

    qSemaphore_ = sem_open (name_.c_str(), O_RDWR | O_CREAT, 
                           S_IRWXU, 1);

    if (qSemaphore_ == SEM_FAILED) {
        printf("Failed to open semaphore! terminating!\n"); 
        throw;
    }
}

MsgQueueImpl::~MsgQueueImpl()
{
    assert (mq_close (qd_) == 0);
    assert (mq_unlink (name_.c_str()) == 0);
    assert (sem_close(qSemaphore_) == 0);
    assert (sem_unlink(name_.c_str()) == 0);
}


bool MsgQueueImpl::rcvMsg (void * const msg, uint32_t &len)
{
    
    bool ret = true;
    
    int32_t numOfBytesRcvd = mq_receive(qd_, static_cast<char *>(msg),
                             msgSize_, NULL);

    if (numOfBytesRcvd == -1) {
        len = 0;
        ret = false;
    } else {
        len = numOfBytesRcvd;
    }

    return ret;
}

bool MsgQueueImpl::sendMsg (void const * msg, uint32_t len,
                            MsgPriority prio)
{
    enum
    {
        POSIX_MAX_PRIO = 31,
        POSIX_MEDIUM_PRIO = 16,
        POSIX_LOW_PRIO    = 0
    };

    uint32_t mq_prio = POSIX_LOW_PRIO; 
    switch (prio)
    {
    case LOW:
        mq_prio = POSIX_LOW_PRIO;
    break;

    case MEDIUM:
        mq_prio = POSIX_MEDIUM_PRIO;
    break;

    case HIGH:
        mq_prio = POSIX_MAX_PRIO;
    break;

    default:
        assert (false);
    break;
    } 
 
    assert (len <= msgSize_);
    
    bool ret = true;
    int32_t sendStatus;

    // Atomic write to msg Queue
    sem_wait (qSemaphore_);
    sendStatus = mq_send (qd_, static_cast<const char *>(msg), 
                          len, mq_prio); 
    sem_post (qSemaphore_);

    if ( sendStatus != 0 ) {
        ret = false;
    }

    return ret;
}


uint32_t MsgQueueImpl::getNumMsgs()
{
    mq_attr myAttr;
    
    // sem_wait (qSemaphore_);
    mq_getattr (qd_, &myAttr);
    // sem_post (qSemaphore_);

    return static_cast<uint32_t>(myAttr.mq_curmsgs);
}

std::string MsgQueueImpl::getName()
{
    std::string retString = name_;
    
    retString.erase(0, 1);
    return retString;
}

int32_t MsgQueueImpl::getDescriptor()
{
    return static_cast<int32_t>(qd_);
}

std::unordered_map<std::string, MsgQueue *> queueRegistery;

MsgQueue & MsgQueue::getInstance(const std::string queueName,
                                 const uint32_t msgSize,
                                 const uint32_t maxNumMsgs)
{
    // Check if the queue does not exist
    if (queueRegistery.find(queueName) == queueRegistery.end()) {
        queueRegistery[queueName] = new MsgQueueImpl(queueName,
                                                     msgSize,
                                                     maxNumMsgs);
    } 

    return *queueRegistery[queueName];
}

bool MsgQueue::removeInstance (const std::string queueName)
{
    bool entryFound = true;

    if (queueRegistery.find (queueName) == queueRegistery.end()) {
        entryFound = false;
    } else {
        delete queueRegistery[queueName];
        queueRegistery.erase(queueName);
    }

    return entryFound;
}


