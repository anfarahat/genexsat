#include "narss_thread.h"

#include <pthread.h>
#include <sched.h>

class PosixThread : public Thread
{
public:
    PosixThread (std::string threadName,
                 void * (*threadFunc)(void *),
                 void * args,
                 Policy policy,
                 uint32_t priority, 
                 Affinity affinity);

    virtual ~PosixThread(){}
    void join() override;
    uint32_t getPriority() override;
    Policy getPolicy() override;
    std::string getName() override;

    bool active_;

private:
    std::string threadName_;
    uint32_t priority_;
    Policy policy_;
    Affinity affinity_;
    // pthread specific
    pthread_t pthread_;
    
};

PosixThread::PosixThread (std::string threadName,
                          void * (*threadFunc)(void *),
                          void * args,
                          Policy policy,
                          uint32_t priority, 
                          Affinity affinity)
: active_(false),
  threadName_(threadName),
  priority_(priority),
  policy_(policy),
  affinity_(affinity)
{
    // These are the thread attributed
    // should be set according to 
    pthread_attr_t attr;
    int ppolicy;
    sched_param sparam;
   
    ppolicy = (policy == POLICY_NORMAL)? SCHED_OTHER : SCHED_RR;
    
    sparam.sched_priority = sched_get_priority_max(ppolicy) - priority;
  

    pthread_attr_init (&attr);

    // Set policy (whether real-time or not)    
    if (0!=pthread_attr_setschedpolicy(&attr, ppolicy)) {
        printf ("setpolicy failed!\n");
        throw;
    }

    // Set priority
    if (0!=pthread_attr_setschedparam(&attr, &sparam)) {
        printf ("setschedparam failed!\n");
        throw;
    }
    
    // Do not inherit attributed from parent thread
    if (0 != pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)) {
        printf ("setinherit failed!\n");
        throw;
    }
    
    // Compete with all threads running on the system
    if (0!=pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM)) {
        printf ("setscope failed!\n");
        throw;
    }
    
    // set Detached state
    if (0!=pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) {
        printf ("setdetached failed!");
        throw;
    }

    // create the thread here!
    if (pthread_create (&pthread_, &attr,
                        threadFunc, args) != 0)
    {
        // Fail since thread failed to create?
        printf ("pthread creation failed\n");
        throw;
    } else {
        active_ = true;
    }

    pthread_attr_destroy(&attr);
}


Thread * Thread::create (std::string threadName,
                         void * (*threadFunc)(void *),
                         void * args,
                         Policy policy,
                         Priority priority,
                         Affinity affinity)
{
    return new PosixThread (threadName, threadFunc,
                            args, policy, priority, affinity);
}

bool Thread::terminate (Thread * threadPtr)
{
    PosixThread * thiz = static_cast<PosixThread *>(threadPtr);
    bool ret = false;

    if (thiz->active_ == false) {
        delete thiz;
        ret = true;
    }

    return ret;
}

void PosixThread::join()
{
    if (&pthread_ != nullptr) {
        pthread_join(pthread_, nullptr);
        active_ = false;
    }
}

uint32_t PosixThread::getPriority()
{
    return priority_;
}

Thread::Policy PosixThread::getPolicy()
{
    return policy_;
}

std::string PosixThread::getName()
{
    return threadName_;
}
