#include "narss_timer.h"

#include <cassert>
#include <sys/timerfd.h>
#include <unistd.h>
#include <cstdio>
class TimerImpl : public Timer
{
public:
    TimerImpl(TimerType type, uint32_t intervalInMs)
    : fd_(timerfd_create(CLOCK_MONOTONIC, 0)),
      type_(type),
      intervalInMs_(intervalInMs) {
        if (fd_ == -1) {
            printf("Bad timer file descriptor, terminating!");
            throw; // Throwing exception
                              // since we have no guarantees
                              // from the environment.
        }
        checkInvariant();
    }
    ~TimerImpl() {
        close(fd_);
    }

    void start() override;
    void start(uint32_t intervalInMs) override;
    void stop() override;
    void waitEvent() override;
    int32_t getDescriptor() override;
    void * defaultCallback(void * args) override;

private:
    constexpr static uint32_t NSEC_IN_MSEC = 1000000;
    constexpr static uint32_t MSEC_IN_SEC = 1000;
 
    int32_t fd_;
    TimerType type_;
    uint32_t intervalInMs_;

    
    inline void checkInvariant() {
        assert (fd_ > -1);
        assert (intervalInMs_ > 0);
        assert (type_ == PERIODIC || type_ == ONE_SHOT);
    }
};


void TimerImpl::start()
{
    start(intervalInMs_);
    checkInvariant();
}

void TimerImpl::start(uint32_t intervalInMs)
{
    intervalInMs_ = intervalInMs;
 
    time_t intervalSecs = intervalInMs_ / MSEC_IN_SEC;
    long intervalNSecs  = (intervalInMs_ - intervalSecs * MSEC_IN_SEC) * NSEC_IN_MSEC; 
    itimerspec newValue, oldValue;
    
    newValue.it_interval.tv_sec = 0;
    newValue.it_interval.tv_nsec = 0; // by default it is one shot; so period is zero.

    newValue.it_value.tv_sec = intervalSecs; // Forthe first interval
    newValue.it_value.tv_nsec = intervalNSecs;// For the first interval
    
    if (type_ == PERIODIC) {
        // set period otherwise
        newValue.it_interval.tv_sec = intervalSecs;
        newValue.it_interval.tv_nsec = intervalNSecs;
    }

    if (timerfd_settime(fd_, 0, &newValue, &oldValue) == -1) {
        printf("settime failed! terminating!");
        throw;
    }
    checkInvariant();
}

int32_t TimerImpl::getDescriptor()
{
    return fd_;
}

void * TimerImpl::defaultCallback(void * args)
{
    waitEvent(); 
    return args;
}

void TimerImpl::waitEvent()
{
    int64_t s;

    if (read (fd_, &s, sizeof(s)) == -1) {
        printf("read timer failed! terminating!\n");
        throw;
    }
    if (s != 1) printf ("Timer missing events! number of received events = %d\n", (uint32_t)s);
}
void TimerImpl::stop()
{
    itimerspec newValue, oldValue;
    
    newValue.it_interval.tv_sec = 0;
    newValue.it_interval.tv_nsec = 0;
    
    newValue.it_value.tv_sec = 0;
    newValue.it_value.tv_nsec = 0;

    if (timerfd_settime(fd_, 0, &newValue, &oldValue) == -1) {
        printf("stop(): settime failed! Terminating!\n");
        throw;
    }

    checkInvariant();
}
Timer & Timer::create(TimerType type, uint32_t intervalInMs)
{
    // This is internal, so preconditions
    // should be satisfied by the caller, no exceptions.
    // 
    assert(type == PERIODIC || type == ONE_SHOT);
    assert(intervalInMs > 0);

    return *(new TimerImpl(type, intervalInMs));
}

void Timer::remove(Timer & timer)
{
    delete &timer;
}
