////////////////////////////////////
/// File: narss_msg_xmit.cpp
///
/// Description: Infrastructure for sending
///              messages to processes or
///              across serial port
////////////////////////////////////

#include "narss_msg_xmit.h"
#include "narss_msg_queue.h"

#include <string>
#include <unordered_map>

class TransmitMsgImpl : public TransmitMsg
{
public:
    TransmitMsgImpl ()
    : queueName_ { {ADC, "adcsApp"},
                   {OBC, "obcApp"},
                   {TLM, "tlmApp"},
                   {ROUTER, "routerApp"} }
    {}
    bool xmitMsg (uint8_t const * msg,
                  uint32_t msgLength,
                  Destination dest) override;

private:

// Here we implement all the msg queues and all the device specific
// serial port interfaces.
    std::unordered_map<uint32_t, std::string> queueName_;
};


TransmitMsg & TransmitMsg::getSingleton()
{
    static TransmitMsgImpl transmitImplS;

    return transmitImplS;
}

bool TransmitMsgImpl::xmitMsg(uint8_t const *msg,
                              uint32_t msgLength,
                              Destination dest)
{
    return MsgQueue::getInstance(queueName_[(uint32_t)dest]).sendMsg((void *)msg,
                                                                     msgLength, 
                                                                     MsgQueue::HIGH); 
}
