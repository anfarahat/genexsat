#pragma once

#include "narss_reactive_object.h"

#include <cstdint>
#include <string>

class SerialPort : public ReactiveObject
{
public:
    virtual ~SerialPort(){}
    virtual bool readSerialData (uint8_t * const msg, uint32_t & len) = 0;
    virtual  bool writeSerialData (uint8_t * const msg, uint32_t len) = 0;

    enum SerialPortName {
        COM1P,
        COM2P,
        COM3
    };

    static SerialPort & getInstance(SerialPortName portName);
    static bool removeInstance (const std::string queueName);

    virtual bool getInitStatus() = 0;
};
