#pragma once
#include <string>
#include <cstdint>


class Thread
{
public:
    enum Policy
    {
        POLICY_NORMAL = 0,
        POLICY_REAL_TIME = 1
    };

    enum Affinity
    {
        CORE_ZERO  = 1,
        CORE_ONE   = 2,
        CORE_TWO   = 4,
        CORE_THREE = 8
    };

    typedef int32_t Priority; 
   
    static Thread * create (std::string threadName,
                            void * (*threadFunc)(void*),
                            void * args = nullptr,
                            Policy policy = POLICY_NORMAL,
                            Priority priority = 0,
                            Affinity affinity = static_cast<Affinity>(~0));

    static bool terminate (Thread * threadPtr);

    virtual void join() = 0;
    virtual uint32_t getPriority() = 0;
    virtual Policy getPolicy() = 0;
    virtual std::string getName() = 0;
};
