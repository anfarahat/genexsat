#pragma once

#include <iostream>

class GPIO
{
public:
    /*
     * Default Constructor
     */
    GPIO();
    /*
     * Default Destructor
     */
    ~GPIO();

    typedef enum
    {
        GPIO_DIR_IN = 0,
        GPIO_DIR_OUT_INIT_LO,
        GPIO_DIR_OUT_INIT_HI
    }GPIO_Direction_t;
    /* return -1/-2 for fail and 0 for success
     *  Set direction to 2 = high output, 1 low output, 0 input */
    virtual int gpio_set_direction(int gpio, GPIO_Direction_t dir)=0;
    /* return -1 for failed operation or 0 for success operation
     * Single GPIO read */
    virtual int gpio_read(int gpio)=0;
    /* return -1 for failed operation or 0 for success operation
     * Set GPIO to val (1 = high) */
    virtual int gpio_write(int gpio, int val)=0;
    /*
     * get singleton
     */
    static GPIO & getSingleton();

};
