#pragma once

#include "narss_reactive_object.h"

enum TimerType
{
    PERIODIC,
    ONE_SHOT
};


class Timer : public ReactiveObject
{
public:
    /////////////////////////////////////////////////////////////////
    ////  Creates a timer and returns a reference
    ////  to that timer.
    /////////////////////////////////////////////////////////////////
    static Timer & create(TimerType type, uint32_t periodInMs);// do no accept intervals
    
    //////////////////////////////////////////////////////////////
    /// Removes the timer created
    //////////////////////////////////////////////////////////////                                                                  
    static void remove(Timer & timer);

    // Starts the timer based of the interval setting
    // provided at creation time.
    virtual void start() = 0;
    virtual void start(uint32_t periodInMs) = 0; // This allows changing the interval of the same timer.

    virtual void stop() = 0;
    virtual void waitEvent() = 0;
};
