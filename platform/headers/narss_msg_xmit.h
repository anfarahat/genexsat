#pragma once

#include <cstdint>


enum Destination
{
    COM0,
    COM1,
    ADC,
    TLM,
    OBC,
    ROUTER,
    MAX_NUM_OF_DEST
};

class TransmitMsg
{
public:
    virtual bool xmitMsg (uint8_t const * msg, 
                  uint32_t msgLength, 
                  Destination destinationName) = 0;
    // Destination could be any of these:
    // COM0, COM1, ADC, TLM, CMD, 
    static TransmitMsg & getSingleton();
};
