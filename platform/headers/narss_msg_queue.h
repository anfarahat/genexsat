#pragma once
#include <string>
#include <stdint.h>

#include "narss_reactive_object.h"

class MsgQueue : public ReactiveObject
{
public:
    constexpr static uint32_t MAX_MSG_SIZE = 0x408;
    constexpr static uint32_t MAX_NUM_MSG  = 64;


    enum MsgPriority
    {
        LOW,
        MEDIUM,
        HIGH
    };

    virtual ~MsgQueue(){}
    ////////////////////////////////////////////////////////
    /// Method: rcvMsg
    ///
    /// Input: msg, pointer to message buffer
    ///        len, reference to the length field
    ///
    /// Output: *msg, contents of the received message
    ///          len, value of the length of the received msg
    ///
    /// Description: populates the buffer with the received
    ///              message.
    ///
    /// Returns:     true if and only if reception succeeds.
    /////////////////////////////////////////////////////////    
    virtual bool  rcvMsg (void * const msg, uint32_t &len) = 0;
    
    /////////////////////////////////////////////////////////
    /// Method: sendMsg
    ///
    /// Input:  msg, pointer to msg buffer containing the
    ///         frame to be transmitted.
    ///         len, size in bytes of  the message to be
    ///         transmitted.
    ///         prio, priority of the message to be transmitted.
    ///
    /// Description: Sends the contents of *msg to the message
    ///              queue with length len, and with a priority prio.
    ///
    /// Returns:     true if and only if transmissions is successful.
    /////////////////////////////////////////////////////////////////
    virtual bool sendMsg (void const * msg, uint32_t len, 
                          MsgPriority prio) = 0;
    virtual uint32_t getNumMsgs() = 0;
    virtual std::string getName() = 0;
    static MsgQueue & getInstance(const std::string queueName,
                                  const uint32_t msgSize = MAX_MSG_SIZE,
                                  const uint32_t maxNumMsgs = MAX_NUM_MSG);
    static bool removeInstance (const std::string queueName);
};
