
#pragma once
#include <cstdint>
// This file needs to move down to platform
// This interface can only be used
// by functions in event reactor.
// This way, it can directly use a reference to Reactive Object
// to just access the file descriptor of a specific reactive object.

class ReactiveObject
{
public:
    virtual ~ReactiveObject(){}   
    virtual int32_t getDescriptor() = 0;
    virtual void * defaultCallback(void * args) {return args;} // needed to define
                                                              // default behavior for a
                                                              // a particular reactor.
};
