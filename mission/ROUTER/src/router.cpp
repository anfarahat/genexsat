#include "router.h"
#include "narss_msg_xmit.h"
#include "obc_cmd_defs.h"
#include "adc_cmd_defs.h"
#include "tlm_cmd_defs.h"

#include <unordered_map>


// This is where all the command
// codes are defined for each app

enum
{
    REQUEST_MSG,
    REPLY_MSG
};



class RouterImpl : public Router
{
public:
    RouterImpl();

    uint16_t getFrameLength(uint32_t msgCode, bool msgDirection) override;

    bool routeMsg(InternalMsg & msg) override;


private:
    // Routing Table As an unordered map
    // Key is a CmdCode
    // Value is the subsystem Enum
    std::unordered_map<uint32_t, Destination> routingTable;

};

#define STRIP_LEN(code)  (code & (~(0xFF << 24)));

// Routing table has to be initialized
// Based on the command codes defined
// in each app. To that end, we need to export
// the command code definitions outside their
// corresponding cpp files into header defs.

RouterImpl::RouterImpl() 
: routingTable{

     // OBC Codes

    // TLM Codes

    // ADCS Codes
    }
{}


bool RouterImpl::routeMsg(InternalMsg & msg)
{
    
    return TransmitMsg::getSingleton().xmitMsg (reinterpret_cast<uint8_t*>(&msg), 
                                        msg.header.length + sizeof(msg.header), 
                                        routingTable [msg.header.cmdCode()]);
}


uint16_t RouterImpl::getFrameLength(uint32_t msgCode, bool msgType)
{
    msgCode = msgCode;
    msgType = msgType;
    uint16_t actualLength = 0;
    return actualLength;
}



Router & Router::getInstance()
{
    static RouterImpl routerInstanceS;
    return routerInstanceS;
}
