#include <cassert>
#include "cmd_manager.h"
#include "tlm_cmd_defs.h"
#include "router.h"
#include "tlm_controller.h"
#include "tlm_sampler.h"
#include "tlm_frame_defs.h"

#define MAX_TLM_FRAME 256 

class CmdMgrTlm:public CmdMgr
{
public:

    virtual ~CmdMgrTlm(){} 

    void initMap() override;

private:
    // Declare here each reactor to a command (command executor)
    // of type: void * (commandName)(uint32_t len, void * args);
}; 

// Implement here each command reactor function
// For example:
// void * CmdMgrAdc:: commandName(uint32_t len, void * args)
//{
//}


void CmdMgrTlm::initMap()
{
    // For example, for each supported command
    // add a line of the form.
    // commandMap_[CMD_NAME] = &CmdMgrAdc::commandName;
 
//    assert (MAX_NUM_TLM_CMD_RESPONSE == commandMap_.size());
    
};


// Factory method

CmdMgr & CmdMgr::getSingleton()
{
    static CmdMgrTlm cmdMgrTlmS;
    return cmdMgrTlmS;
}

