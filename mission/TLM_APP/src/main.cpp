#include "application.h"
#include "obt_thread.h"

#include <cstdio>


void * tlmSamplingFn(void *)
{
    // Thread should call the sampling function
    // based on the measurement program.
    static uint32_t count = 0;
    if (count % 10 == 0) {
        printf ("TLM Thread Active. Thread index = %d\n", count);
    }
    count++;
}

int main (int argc, char * argv[])
{
    Application app("tlmApp");

    ThreadId id = ObtThread::getSingleton().registerThread( tlmSamplingFn, 1000);
    ObtThread::getSingleton().start(id, 3);

    app.run();

    ObtThread::getSingleton().stop(id);

    return 0;
}

