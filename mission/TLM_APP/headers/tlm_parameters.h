#pragma once

#include <stdint.h>

struct tlmParameters;

namespace tlmMissionParameters
{
    constexpr uint8_t NUM_OF_ANALOG_SENSORS = 16;
    constexpr uint8_t NUM_OF_TEMP_SENSORS = 16;
    constexpr uint8_t NUM_OF_DIGI_SENSORS = 32;

    constexpr uint8_t PWR_GET_PWR_Line1_DATA_LENGTH = 212;
    constexpr uint8_t PWR_GET_PWR_Line2_DATA_LENGTH = 228;
    constexpr uint8_t PWR_GET_PWR_Line3_DATA_LENGTH = 164;
    constexpr uint8_t PWR_GET_PWR_Line4_DATA_LENGTH = 192;
    constexpr uint8_t PWR_GET_STDR_TLM_DATA_LENGTH = 24;
    constexpr uint8_t PWR_GET_EXTL_TLM_DATA_LENGTH = 64;

    constexpr uint8_t TTC_TLM_DATA_LENGTH = 4;

    constexpr uint8_t PPHD_STANDARD_TLM_DATA_LENGTH = 40;
    constexpr uint8_t PPHD_Power_TLM1_DATA_LENGTH = 160;
    constexpr uint8_t PPHD_POWER_TLM2_DATA_LENGTH = 96;
    constexpr uint8_t PPHD_POWER_TLM3_DATA_LENGTH = 112;
    constexpr uint8_t PPHD_SPL_CAM_DATA_LENGTH = 40;

    constexpr uint8_t HP_OBC_TLM_DATA_LENGTH = 9;

    constexpr uint8_t AOCS_TLM_PACKET_NUM1_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM2_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM3_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM4_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM5_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM6_DATA_LENGTH = 100;
    constexpr uint8_t AOCS_TLM_PACKET_NUM7_DATA_LENGTH = 100;

    
    constexpr uint8_t NEXSAT_WINDOW_SIZE = 7;
    constexpr uint32_t NEXSAT_TLM_EMPTY_FRAME_CODE = 0xB0A07;
    constexpr uint8_t NEXSAT_TLM_ID = 0X07;
    constexpr uint8_t NEXSAT_TLM_EMPTY_FRAME_C1 = 0xA;
    constexpr uint8_t NEXSAT_TLM_EMPTY_FRAME_C2 = 0xB;

    constexpr uint8_t NEXSAT_NO_TLM_FRAMES = 23;
    constexpr uint8_t NEXSAT_TLM_FRAME_CODE_LEN = 4;
    constexpr uint8_t NEXSAT_TLM_PAYLOAD_INDEX = 3;
    constexpr uint32_t NEXSAT_MAX_TLM_PAYLOAD_LEN = 255;
    constexpr uint32_t NEXSAT_RECORDED_TLM_FILE_SIZE = 0x39800; //230*1024 = 230Kbytes

    void initTlmParameters(tlmParameters* tlmParametersNexsat);
    
}



