#include "application.h"

int main (int argv, char * argc[])
{
    Application app("routerApp");

    app.run();

    return 0;   
}
