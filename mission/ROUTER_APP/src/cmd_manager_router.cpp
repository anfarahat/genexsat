#include <cassert>

#include "cmd_manager.h"
#include "router.h"
#include "narss_serial_port.h"

#include "obc_cmd_defs.h"
#include "adc_cmd_defs.h"


enum {
// Please include the code
// of OBC command code here
//  For example:
    // CMD_NAME = CMD_CODE,
};

class CmdMgrRouter : public CmdMgr
{
public:
//    CmdMgrRouter() {
//    }

    virtual ~CmdMgrRouter(){} 

    void initMap() override;

private:


};


// Please implement this function
// according to the set of supported commands
// and their functions
void CmdMgrRouter::initMap()
{
    //OBC CMDS

}


// Factory method

CmdMgr & CmdMgr::getSingleton ()
{
    static CmdMgrRouter cmdMgrRouterS;
    return cmdMgrRouterS;
}
