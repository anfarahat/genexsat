#include "obt_thread.h"
#include "onboard_time.h"

int main (int argc, char *argv[])
{
    // Fork the onboard real-time thread here
    // to run every 10 ms.
    OnboardTime::getSingleton(true); // to set master thread.
    ObtThread::getSingleton().start();

    // Never terminates since start waits for joining. 
    // Join here
    return 0;
}

