#pragma once

#include "obt_thread.h"

class TExecThread
{
public:
    TExecThread(){}

    virtual void startTTExecThread(void) = 0;

    virtual ThreadId getTTExecThreadId(void) = 0;

    static TExecThread & getSingleton();
};
