#include "application.h"
#include "obt_thread.h"
#include "onboard_time.h"
#include "timed_cmd_exec_thread.h"
#include "narss_thread.h"
#include <cstdio>
#include <string>
#include <unistd.h>

void * threadFn(void * args)
{
    uint32_t id = *static_cast<uint32_t *>(args);

    for (;;)
    {
        printf ("This is thread %d\n", id);
        usleep(10000);
    }
}
int main (int argv, char * argc[])
{
    Application app("obcApp");
    OnboardTime & obt = OnboardTime::getSingleton(false); 
    TExecThread::getSingleton().startTTExecThread();

    constexpr uint32_t MAX_NUM_THRDS = 30;

    Thread * thrdPtrs[MAX_NUM_THRDS];

    uint32_t args [MAX_NUM_THRDS];
    
    for (uint32_t index = 0; index < MAX_NUM_THRDS; ++index)
    {
        args [index] = index;

        std::string thrdName = "thrd" + std::to_string(index);
        thrdPtrs [index] = Thread::create(thrdName, threadFn, &args[index]);
    }

    app.run();
    ObtThread::getSingleton().stop(TExecThread::getSingleton().getTTExecThreadId());

    for (uint32_t index = 0; index < MAX_NUM_THRDS; ++index)
    {
        thrdPtrs[index]->join();
    }

    return 0;
}
