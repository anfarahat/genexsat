#include "timed_cmd_exec_thread.h"
#include "timed_command.h"

#include <cstdio>

class TTExecThreadImpl : public TExecThread
{
    public:

    void startTTExecThread(void) override;

    ThreadId getTTExecThreadId(void) override;

    private:

    const uint32_t tTThreadPeriod = 100; // ms
    const uint8_t tTThreadPriority = 2;  // highest priority is 0
    ThreadId tTExecThreadId;

    static void * tTExecThreadFn(void *);
};


void TTExecThreadImpl::startTTExecThread(void)
{
    tTExecThreadId = ObtThread::getSingleton().registerThread(tTExecThreadFn, tTThreadPeriod);
    ObtThread::getSingleton().start(tTExecThreadId, tTThreadPriority);
}


void * TTExecThreadImpl::tTExecThreadFn(void *)
{
    static uint32_t count = 0;
    if ((count % 100) == 0) {
       printf ("Timed Command Thread is Active! its index is %d\n", count);
    }
    count++;

    bool cmdExecStatus = TimedCommand::getSingleton().executeCommand();
}

ThreadId TTExecThreadImpl::getTTExecThreadId(void)
{
    return tTExecThreadId;
}

TExecThread & TExecThread::getSingleton()
{
    static TTExecThreadImpl tTExecThread;
    return tTExecThread;
}

