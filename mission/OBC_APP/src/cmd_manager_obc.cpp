#include <cassert>
#include <cstring>

#include "cmd_manager.h"
#include "onboard_time.h"
#include "timed_command.h"
#include "obc_cmd_defs.h"
#include "router.h"
#include "narss_msg_xmit.h"
#include "obc_frame_defs.h"
#include "tlm_frame_defs.h"

// Has to be a Cpp file
class CmdMgrObc : public CmdMgr
{
public:
    //CmdMgrAdc() {
    //    this->initMap();
    //}

    virtual ~CmdMgrObc(){} 

    void initMap() override;

private:
    // Declare here each reactor to a command (command executor)
    // of type: void * (commandName)(uint32_t len, void * args);

};

// Implement here each command reactor function
// For example:
// void * CmdMgrObc:: commandName(uint32_t len, void * args)
//{
//}

// Please implement this function
// according to the set of supported commands
// and their functions
void CmdMgrObc::initMap()
{
    // For example, for each supported command
    // add a line of the form.
    // commandMap_[CMD_NAME] = &CmdMgrObc::commandName;

//    assert (MAX_NUM_CMDS == commandMap_.size());
}


// Factory method

CmdMgr & CmdMgr::getSingleton ()
{
    static CmdMgrObc cmdMgrObc;
    return cmdMgrObc;
}
