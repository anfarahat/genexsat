#pragma once
#include <string>

class ApplicationIf
{
friend class Application;
friend void * msgQueueCallback(void *);
private:
    virtual std::string getName() = 0;

    static ApplicationIf * getInstance(std::string appName);
    static void removeInstance(ApplicationIf *);
   
    virtual void run() = 0;    
};

class Application
{
public:
    Application(std::string appName) 
    : appPtr (ApplicationIf::getInstance(appName)){}
    void run() {
        appPtr->run();
    }    
    ~Application() {
        ApplicationIf::removeInstance(appPtr);
    }
private:
    ApplicationIf * appPtr;
};

