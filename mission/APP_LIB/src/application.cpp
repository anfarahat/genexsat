#include <string>
#include <cstdint>
#include "cmd_manager.h"
#include "narss_msg_queue.h"
#include "event_reactor.h"
#include "router.h"
#include "application.h"

static ApplicationIf *appS = nullptr;
void * msgQueueCallback (void *)
{
    // Read msgs from msg queue
    // and execute their corresponding command
    // one-by-one
    // This imposes a strong requirement
    // on command execution not to
    // spend time executing commands.
   
    // Get a reference to the message queue
    MsgQueue & inputQueue = MsgQueue::getInstance(appS->getName()); 
  
    // Execution loop
    do {
        uint32_t len;
        InternalMsg msg;
    
        inputQueue.rcvMsg((void *)&msg, len);
        CmdCode_t code = msg.header.cmdCode();
        CmdMgr::getSingleton().executeCmd (code, &msg);
    } while (inputQueue.getNumMsgs() > 0);
}

class ApplicationImpl : public ApplicationIf {
friend void * msgQueueCallback(void *);
public:
    ApplicationImpl(std::string name)
    : appName_ (name) {}

    void run() override;
    std::string getName() override {
        return appName_;
    }

private:
    std::string appName_;
};


void ApplicationImpl::run()
{
    try
    {
        CmdMgr::getSingleton().initMap();

        // Instantiate Main Component

        // The following code needs to move inside an application context
        // However, for now, we keep it here.
        MsgQueue & msgQueue = MsgQueue::getInstance(appName_);

        // Now pass it down to the reactor
        EventReactor::getSingleton().registerCallback (msgQueue,
                                                       msgQueueCallback); 

        // run event reactor: function should never return.
        EventReactor::getSingleton().runEventLoop();
        
 
        // Remove MsgQueue.
        MsgQueue::removeInstance(appName_);
    }
    catch (...) {
        MsgQueue::removeInstance(appName_);
    }
}


ApplicationIf *  ApplicationIf::getInstance(std::string appName)
{
    if (appS == nullptr) {
        appS = new ApplicationImpl(appName);
    }

    return appS;
}

void ApplicationIf::removeInstance(ApplicationIf * app)
{
    if (app != nullptr) {
        delete app;
    }
}
