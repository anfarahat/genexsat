set (SOURCES
     src/application.cpp
    )

set (HEADERS
     headers/application.h
    )

add_library (APP_LIB
             ${SOURCES}
             ${HEADERS})

target_link_libraries (APP_LIB
                       PRIVATE platform event_reactor ROUTER CMD)
