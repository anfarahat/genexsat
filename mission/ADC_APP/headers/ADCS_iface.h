#ifndef _ADCS_IFACE_H
#define _ADCS_IFACE_H

#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
 extern "C" {
#endif

//#define ADCS_L_DBL


#define actuator_array_len      64     
#define sensor_array_len        128
#define tlm_array_len           140
#define gains_array_len         1500
#define rvt_array_len           110
#define sensors_address_lowByte 0x00
#define sensors_address_highByte 0x0b
#define actuators_address_lowByte 0x00
#define actuators_address_highByte 0x0e  

typedef enum
{
    ADCSDETMB =1,
    ADCSSTDBY =2,    
    ADCSIMG =3,
    ADCSER=4,
}ADCSMode;

typedef struct AD_t
{
    ADCSMode adcs_mode;
    bool set_adcs_mode_flag;
    bool shift_time_flag;
}adcs_t;

//global variables
extern bool sensorFlag, actuatorFlag, rvtFlag, gainFlag;
extern uint8_t sensor_array[sensor_array_len];
extern uint8_t actuator_array[actuator_array_len];
extern uint8_t adcs_tlm_array[tlm_array_len];
extern char gains_array[gains_array_len];
extern uint8_t rvt_array[rvt_array_len];
extern char ADCS_Next_Mode, ADCS_Current_Mode;
extern adcs_t adcs_state;
extern ADCSMode adcs_req_mode, adcs_cur_mode;

//ADCs abstractions
void adcs_init(char *g_array);
void adcs_exe(adcs_t *adcs_struct, uint8_t *s_array, bool *s_flag, \
              uint8_t *a_array, bool *a_flag, char *ADCS_Current_Mode, \
              char *ADCS_Next_Mode, uint8_t *rvt_array, bool *rvt_flag, uint8_t *tlm_array );

#ifdef __cplusplus
}
#endif

#endif /* _ADCS_IFACE_H */
