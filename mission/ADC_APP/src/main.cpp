#include "application.h"
#include "obt_thread.h"

#include <cstdio>

void * adcsThrdFn(void *)
{
    static uint32_t count = 0;
    if ((count % 10) == 0) {
        printf("ADCS Thread Active. Thread index = %d\n", count);
    }
    count++;
}

int main (int argv, char * argc[])
{
    Application app("adcsApp");

    ThreadId id = ObtThread::getSingleton().registerThread(adcsThrdFn, 1000);
    ObtThread::getSingleton().start(id, 3);
    
    app.run();
    
    ObtThread::getSingleton().stop(id);
    return 0;
}
