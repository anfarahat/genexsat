#include <unordered_map>
#include <cstring>

#include "transmit_ADCS_Frames.h"
#include "adc_cmd_defs.h"
#include "narss_msg_xmit.h"
#include "adc_frame_defs.h"
#include "router.h"



class TxADCSFrame : public TrxADCSFrame
{
public:
    TxADCSFrame();
    // void transmitADCSFrame(AdcsFrameType frameType, uint8_t length, AdcsFrame adcsFrameData);

private:
    std::unordered_map<uint32_t, AdcsInputFrame> adcsRoutingTable;
};


TxADCSFrame::TxADCSFrame()
: adcsRoutingTable{

    }
{}

/*void TxADCSFrame::transmitADCSFrame(AdcsFrameType frameType, uint8_t length, AdcsFrame adcsFrameData)
{
    InternalMsg adcsFrame;
    uint8_t cmdLength, i;

    cmdLength = length + sizeof(adcsFrame.header);

    adcsFrame.header.setHeader(static_cast<uint32_t>(adcsRoutingTable[frameType]), length);

    memcpy(adcsFrame.body.data, adcsFrameData.adcsFrame , length);

    TransmitMsg::getSingleton().xmitMsg (reinterpret_cast<uint8_t*>(&adcsFrame), cmdLength, ROUTER);
}*/


TrxADCSFrame & TrxADCSFrame::getInstance()
{
    static TxADCSFrame trxADCSFrame;
    return trxADCSFrame;
}
