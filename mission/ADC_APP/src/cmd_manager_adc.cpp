#include <cassert>

#include "cmd_manager.h"
#include "adc_cmd_defs.h"

#include "router.h"
#include "narss_msg_xmit.h"
#include "adc_frame_defs.h"

// Has to be a Cpp file

class CmdMgrAdc : public CmdMgr
{
public:
    virtual ~CmdMgrAdc(){}

    void initMap() override;

private:
    
};




void CmdMgrAdc::initMap()
{
    // commandMap_ [SET_SENSOR_VALUES_GYRO_REPLY]                  = static_cast<void * (CmdMgr::*)(void *)> (&CmdMgrAdc::setSensorValuesGyroReply);
    
}


// Factory method

CmdMgr & CmdMgr::getSingleton ()
{
    static CmdMgrAdc cmdMgrAdcS;
    return cmdMgrAdcS;
}
