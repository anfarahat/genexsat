

set(TARGET_APP "adcs_app")

set(SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/cmd_manager_adc.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp
            #${CMAKE_CURRENT_SOURCE_DIR}/src/AOCS.cpp
            #${CMAKE_CURRENT_SOURCE_DIR}/src/Telemetry.cpp
            #${CMAKE_CURRENT_SOURCE_DIR}/src/parameters.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/src/transmit_ADCS_Frames.cpp
            )


set (HEADERS 
     #headers/AOCS.h
     #headers/parameters.h
     #headers/Telemetry.h
     headers/ADCS_iface.h
    )

set(LIBS 
    APP_LIB
    CMD
    OBT
    OBT_THRD
    #ADC
    platform)

add_executable (
                ${TARGET_APP} 
                ${SOURCES}
                ${HEADERS}
               )
#set (STDLIB "")
#if (${BUILD_TYPE} MATCHES "cortex-A9" )
#    set (STDLIB -static-libgcc -static-libstdc++)
#endif()
target_link_libraries (${TARGET_APP} PRIVATE ${LIBS}
                                            #${STDLIB}
                                            )



