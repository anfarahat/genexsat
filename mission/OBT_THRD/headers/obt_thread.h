#pragma once
#include <cstdint>

typedef uint32_t ThreadId;
class ObtThread
{
friend int main(int, char**);
public:
    virtual ~ObtThread() {}
 
    // Starts another periodic thread
    // synchronized to the obt
    ///////////////////////////////////////////////////
    /// Returns: ThreadID -- used for starting 
    ///                      the thread.
    ///////////////////////////////////////////////////
    virtual uint32_t registerThread(void * (*threadFunc)(void *),
                            uint32_t periodInTicks = 1) = 0;

    virtual void unRegisterThread(ThreadId threadId) = 0;

    // Starts and stops a registered thread
    // using its Id
    virtual void start (ThreadId threadId, uint8_t priority) = 0;
    virtual void stop (ThreadId threadId) = 0;

    // Blocks for the specified amount of
    // microseconds.
    virtual void waitUs(uint32_t usSleep) = 0; 
    
    static ObtThread & getSingleton();

private:
    // Start OBT Thread
    virtual void start() = 0;
};
