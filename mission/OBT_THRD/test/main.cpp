#include "obt_thread.h"
#include "onboard_time.h"
#include <iostream>
#include <cstdio>

void *thr1(void *)
{

	//std::cout<<"thr1 is running\n";
        printf("thr1 is running, obt = %ld\n", (long int)OnboardTime::getSingleton().getTime());

        if (OnboardTime::getSingleton().getTime() == 50000)
        {
          ObtThread::getSingleton().stop(2);
          ObtThread::getSingleton().unRegisterThread(2);
        } 

}

void *thr2(void *)
{

	printf("thr2 is running, obt = %ld\n", OnboardTime::getSingleton().getTime());

}

int main (int argc, char *argv[])
{
    // Fork the onboard real-time thread here
    // to run every 10 ms.

    // Make sure to register the app as master obt,
    // otherwise, obt thread will not start.
    OnboardTime::getSingleton(true);

    ThreadId id1 = ObtThread::getSingleton().registerThread(thr1, 2000);
    ThreadId id2 = ObtThread::getSingleton().registerThread(thr2, 1000);
    
    printf("id1 = %d, id2 = %d\n", id1,id2); 

    ObtThread::getSingleton().start(id2, 2);
    ObtThread::getSingleton().start(id1, 2);

    ObtThread::getSingleton().start();

    // Never terminates since start waits for joining. 
    // Join here    
    return 0;
}
