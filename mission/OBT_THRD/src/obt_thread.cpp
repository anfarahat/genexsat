#include "narss_thread.h"
#include "narss_timer.h"
#include "obt_thread.h"
#include "onboard_time.h"
#include <cassert>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cstdio>
#include <unordered_map>

class ObtThreadImpl : public ObtThread
{
friend void * obtThreadFn(void *);

public:
   ObtThreadImpl() 
   : obt_ (OnboardTime::getSingleton()),
     threadCount_(obt_.getNumOfThreads()),
     isMaster_(obt_.isMaster()) // should read it from OBT library.
   {
       std::string name = "/semThreadCnt"; // for protection of shared memory
       sem_t * sem = sem_open (name.c_str(), O_RDWR | O_CREAT, 
                           S_IRWXU, 1);
       if (sem == SEM_FAILED) {
           printf ("OBT_THRD: SEM_FAILED in constructor! Terminating!\n");
           throw;
       }
       semThreadCnt_ = sem;
   }

   ~ObtThreadImpl() {
       sem_close(semThreadCnt_);
   }

   ThreadId registerThread(void * (*threadFn)(void *), 
                           uint32_t periodInMs) override;

   void unRegisterThread(ThreadId thId) override;

   void start (ThreadId threadId, uint8_t priority) override;
   void stop (ThreadId threadId) override;
   void waitUs (uint32_t usSleep) override;
   
   // For every threadId, we need to have the following:
   // 1- Period
   // 2- Semaphore for signaling.
   // 3- Thread Function Pointer
   // 4- Last exeuction
   struct ThreadData 
   {
       Thread *thrPntr;
       uint32_t periodInMs;
       sem_t *sem; // For singaling purposes
       void * (*threadFn)(void *);
       uint32_t lastActiveTime;
       bool active;
       ThreadData()
       :  thrPntr(nullptr),
          periodInMs(0),
          sem(nullptr),
          threadFn(nullptr),
          lastActiveTime(0),
          active(false){}
   };

private:
   void start() override;
   void registerRemoteThread(ThreadId threadId, uint32_t periodMs);

   OnboardTime & obt_;
   std::unordered_map<ThreadId, ThreadData *> threadMap_;
   uint32_t & threadCount_;
   sem_t * semThreadCnt_;
   bool isMaster_;
   static void * thrLoopFcn(void *thrId);
};


static ObtThreadImpl * sObtThreadPtr = nullptr;
ObtThread & ObtThread::getSingleton()
{
    static ObtThreadImpl sObtThread;
    if (sObtThreadPtr == nullptr) {
        sObtThreadPtr = &sObtThread;
    }
    return sObtThread;
}


// Should register the thread at
// the master app using shared memory.
// Note that obtThrdFn will not run on a slave app.
// Also we have to guarantee that the app running obtThrd is 
// the master app.
ThreadId ObtThreadImpl::registerThread(void * (*threadFn)(void *),
                                    uint32_t periodInMs)
{
    ///////////////////////////////////////
    ////// Protected Shared Memory ////////
    sem_wait(semThreadCnt_);    
    uint32_t id = threadCount_;
    
    threadCount_++;
    
    // Share the period in shared memory
    // whether we are master or slave
    obt_.getPeriodRef(id) = periodInMs;
    
    sem_post(semThreadCnt_);
    ////// End Protected Shared Memory ////
    ///////////////////////////////////////

    // Local registration
    ThreadData * thisData = new ThreadData;

    thisData->periodInMs = periodInMs;
    thisData->threadFn = threadFn;
    thisData->lastActiveTime = 0;
    thisData->active = false;
    std::string name = "/semThread" + std::to_string(id);
    sem_t * sem = sem_open (name.c_str(), O_RDWR | O_CREAT, 
                           S_IRWXU, 0);
    if (sem == SEM_FAILED) { 
        printf("OBT_THRD: SEM_FAILED in registerThread! Terminating!\n");
        throw ;
    }
    thisData->sem = sem;

    threadMap_ [id] = thisData;

    return id;
}

void ObtThreadImpl::registerRemoteThread(ThreadId threadId, uint32_t periodInMs)
{
    if (threadMap_.find(threadId) == threadMap_.end()) {
        ThreadData * thisData = new ThreadData;
        // note: active and threadFn are not needed
        // in the master obt since the thread
        // details are not handled here.
        thisData->periodInMs = periodInMs;
        thisData->lastActiveTime = 0;
        std::string name = "/semThread" + std::to_string(threadId);
        sem_t * sem = sem_open (name.c_str(), O_RDWR | O_CREAT, 
                                S_IRWXU, 0);
        if (sem == SEM_FAILED) {
            printf("OBT_THRD: SEM_FAILED in register remote thread! terminating!");
            throw;
        }
        thisData->sem = sem;

        threadMap_ [threadId] = thisData;     
    } // if Thread is already there, don't do anything!
}

void ObtThreadImpl::unRegisterThread(ThreadId thId)
{
    std::unordered_map<ThreadId, ObtThreadImpl::ThreadData *>::iterator it;
        
    it = threadMap_.find(thId);
    
    // thread id not found
    assert(it != threadMap_.end());

    ///////////////////////////
    obt_.getPeriodRef(thId) = 0;

    // close semaphore
    sem_close(threadMap_[thId]->sem);

    // delete structure
    delete threadMap_ [thId];

    // erase from list
    threadMap_.erase(thId);
}

// Runs only at the master app
void * obtThreadFn(void * args)
{
    constexpr uint32_t OBT_PERIOD = 10; //milliseconds
    static uint32_t localCountMs = 0; // Needed to synchronize
                                      // threads. OBT Time could
                                      // get updated from ground, but
                                      // this time is absolute.
    // Create a periodic timer of a 10 ms period
    Timer & timerObt = Timer::create(PERIODIC, OBT_PERIOD);
    
    timerObt.start();
    // Infinite Loop
    // inside obt thread.
    timerObt.waitEvent(); // Only wait the first event to advance timer.
    while (true) {
        // 1a) Advance OBT
        localCountMs += OBT_PERIOD;
        
        OnboardTime::getSingleton().advanceTime();

        // 2) Check for all threads in the map
        // and post their corresponding semaphore based on
        // their timing. Well, we'll do this from the shared
        // portion of the memory rather than the map only for the period. 
        // This is necessary since copying shared data to the map is a headache.
        //for (std::unordered_map<ThreadId, ObtThreadImpl::ThreadData *>::iterator it = sObtThread.threadMap_.begin();
        //     it != sObtThread.threadMap_.end(); ++it) {
        for (uint32_t threadIndex = 0; threadIndex < sObtThreadPtr->threadCount_; ++threadIndex) {
            // Register the threadId in map if not available
            sObtThreadPtr->registerRemoteThread(threadIndex, sObtThreadPtr->obt_.getPeriodRef(threadIndex));

            if (sObtThreadPtr->obt_.getPeriodRef(threadIndex) > 0) {
                if (sObtThreadPtr->threadMap_[threadIndex]->lastActiveTime + 
                    sObtThreadPtr->obt_.getPeriodRef(threadIndex) <= localCountMs) {		

                    // Release the semaphore for this thread to signal it to proceed  execution
                    if (sem_post(sObtThreadPtr->threadMap_[threadIndex]->sem) != 0) {
                        printf("sem_post failed inside obt scheduling thread");
                        throw;
                    }
                    sObtThreadPtr->threadMap_[threadIndex]->lastActiveTime = localCountMs; // Needed for remote threads
                }
            }
        }

        // 3) Block on its own timer. Note that this timer
        // actually synchronizes all high priority threads
        // in all processes of interests through semaphores.
        timerObt.waitEvent();
    }
    timerObt.stop();
    Timer::remove(timerObt);
    return args;
}

void * ObtThreadImpl::thrLoopFcn(void *thrId)
{
    ThreadId id = *(ThreadId *) thrId;
    // This information is always available locally
    // in threadMap_, whether we are master app or slave app.
    while (sObtThreadPtr->threadMap_[id]->active) {
        sObtThreadPtr->threadMap_[id]->threadFn(nullptr);     
        sem_wait(sObtThreadPtr->threadMap_[id]->sem);
    }
}

// Starts the obtThreadFn
void ObtThreadImpl::start()
{
    if (!isMaster_) {
        assert (false); // Only call from a master app.
        return;
    }

    // Should fork a thread with OBT Thread contents
    Thread * obtThreadPtr = Thread::create ("ObtThread",
                                            obtThreadFn,
                                            nullptr,
                                            Thread::POLICY_REAL_TIME,
                                            0); // maximum priority
    obtThreadPtr->join();
    // Should never terminate
}

// Starts a synchronized thread, whether on a slave app or on the master app
void ObtThreadImpl::start (ThreadId threadId, uint8_t priority)
{
    std::unordered_map<ThreadId, ObtThreadImpl::ThreadData *>::iterator it;
      
    it = threadMap_.find(threadId);
    
    // thread id not found    
    assert(it != threadMap_.end());
    
    threadMap_[threadId]->active = true;
    Thread * threadPtr = Thread::create (std::to_string(threadId),
                                         thrLoopFcn,
                                         (void *) &(it->first),
                                         Thread::POLICY_REAL_TIME,
                                         priority);
 
    threadMap_[threadId]->thrPntr = threadPtr;
           
}

void ObtThreadImpl::stop (ThreadId threadId)
{
    std::unordered_map<ThreadId, ObtThreadImpl::ThreadData *>::iterator it;
    
    it = threadMap_.find(threadId);

    // thread id not found
    assert(it != threadMap_.end());
    
    threadMap_[threadId]->active = false;
    threadMap_[threadId]->thrPntr->join();
    obt_.getPeriodRef(threadId) = 0;

    //delete thread object
    Thread::terminate(threadMap_[threadId]->thrPntr);
}

void ObtThreadImpl::waitUs (uint32_t usSleep)
{
    usleep (usSleep);
}
